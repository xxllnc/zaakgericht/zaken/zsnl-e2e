import type { PlaywrightTestConfig } from '@playwright/test';
import { devices } from '@playwright/test';

/**
 * See https://playwright.dev/docs/test-configuration.
 */
const config: PlaywrightTestConfig = {
  testDir: './tests',
  timeout: 100 * 1000,
  globalTimeout: process.env.CI ? 1200 * 1000 : 600 * 1000,
  expect: {
    timeout: process.env.CI ? 10000 : 5000
  },
  fullyParallel: true,
  forbidOnly: !!process.env.CI,
  retries: 0,
  workers: process.env.CI ? 1 : undefined,
  reporter: process.env.CI ?
    [
      ['line'],
      ['html', { open: 'never', outputFolder: 'test-reports/' }],
    ] : [
      ['line'],
      ['html', { open: 'never', outputFolder: 'test-reports/' }],
      ['./partial-tests/MyReporter.ts'],
    ],
  use: {
    video: process.env.CI ? 'retain-on-failure' : 'on',
    screenshot: 'only-on-failure',
    actionTimeout: process.env.CI ? 10000 : 5000,
    navigationTimeout: 60 * 1000,
    trace: 'retain-on-failure',
    ignoreHTTPSErrors: true,
  },
  outputDir: './test-screenshots/',
  projects: [
    {
      name: 'chromium',
      use: {
        ...devices['Desktop Chrome'],
      },
    },
    // {
    //   name: 'firefox',
    //   use: {
    //     ...devices['Desktop Firefox'],
    //   },
    // },

    // {
    //   name: 'webkit',
    //   use: {
    //     ...devices['Desktop Safari'],
    //   },
    // },

    /* Test against mobile viewports. */
    // {
    //   name: 'Mobile Chrome',
    //   use: {
    //     ...devices['Pixel 5'],
    //   },
    // },
    // {
    //   name: 'Mobile Safari',
    //   use: {
    //     ...devices['iPhone 12'],
    //   },
    // },

    /* Test against branded browsers. */
    // {
    //   name: 'Microsoft Edge',
    //   use: {
    //     channel: 'msedge',
    //   },
    // },
    // {
    //   name: 'Google Chrome',
    //   use: {
    //     channel: 'chrome',
    //   },
    // },
  ],
};

export const zsnlTestConfig = {
  baseDomain: 'dev.zaaksysteem.nl',
  pageTitle: 'Ontwikkelomgeving :: Zaaksysteem.nl',
  adminUser: 'admin',
  adminPassword: 'admin',
  autobehandelaarUser: 'autobehandelaar',
  autobehandelaarPassword: 'autobehandelaar',
  behandelaarUser: 'gebruiker',
  behandelaarPassword: 'gebruiker',
  beheerderUser: 'beheerder',
  beheerderPassword: 'beheerder',
  bsnDiakriet: '999990743',
  clickDelay: 100,
  clickDelaySlow: 1500,
  shortTimeout: process.env.CI ? 3000 : 1500,
  superShortTimeout: 800,
  longTimeout: process.env.CI ? 15 * 1000 : 10 * 1000,
  superLongTimeout: process.env.CI ? 20 * 1000 : 15 * 1000,
  extreemLongTimeout: 30 * 1000,
  softErrorMessage: 'This could be a timing error!!!',
  typeDelaySlow: 300,
  typeDelay: 50,
  options: {
    locale: 'nl-NL',
    viewport: null,
    deviceScaleFactor: undefined,
    isMobile: undefined,
    screen: {
      width: 1920,
      height: 1280
    },
    recordVideo: {
      dir: 'test-recordings/'
    },
  },
  use: {
    video: 'retain-on-failure',
  }
};

export default config;
