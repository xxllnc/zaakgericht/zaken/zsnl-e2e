# E2e: Getting started

To run the e2e tests local the packages need to be installed and a .env file needs to be created if you want to run on the development environment. The .env is not needed, when you want to run on the local dev environment. You do need to run the admin scripts to fill the db then.

## Steps prior to testing

Clone the zsnl-e2e project and open the project in a terminal

Install the packages:

``` yarn
yarn install --frozen-lockfile
```

Create a .env file:

``` powershell
cp .env.example .env
```

Then edit the .env file and fill in the correct values. You can find them in <https://vault.bitwarden.com/#/login> under the name: Automation test users for development.zaaksysteem.nl

## Start test from shell

``` npx
npx playwright test
```

## Start test from vs-code

Make sure the Playwright extention is installed. it is added as a recommended extention in this project.

Open the test explorer in vs-code and start a test by clicking on the play button.

When no tests are shown somtimes vs-code needs to be restarted.

## Update playwright

Playwright is actively maintained. So new versions are released often. If you want to update the whole project:

``` yarn
yarn upgrade @playwright/test  
# or update all dependencies: 
yarn upgrade-interactive --latest
```
Be carefull that you don't push the changed yarn.lock file to Gitlab. In Gitlab the Playwright Docker container should have the same version as the in your project to be installed Playwright version. 
## New Playwright Docker container version for Gitlab

Updating through yarn updates the yarn.lock and packages.json files. In order for the test to run in GitLab with the same Playwright Docker container version, you also need to manually update and commit the gitlab-ci.yml file. You can find the latest available Playwright version on <https://mcr.microsoft.com/en-us/catalog?search=playwright>.

To set playwright to a specific Playwright version:

``` yarn
yarn upgrade @playwright/test@<version>
```

## Installing yarn

Installation depends on wheter you want to install it on linux/ubuntu or on windows. Follow for example the instructions on:

<https://www.hostinger.com/tutorials/how-to-install-yarn>
