import { BrowserContext, Page, test, expect } from '@playwright/test';
import { baseUrl, beheerderUser, } from '../../e2e_tests';
import {
  personPipDigidLogin, personFormDigidLogin,
  checkPersonDataInForm, logOff,
  openSystemAndLogin, pipLogOff, retrySpoofUntilSucceeded,
} from '../../partial-tests';
import { zsnlTestConfig } from '../../playwright.config';
import { allTestsBeforeAll, checkTextReload, locators, navigateAdministration } from '../../utils';

const { shortTimeout, clickDelay, longTimeout, superLongTimeout, extreemLongTimeout } = zsnlTestConfig;
const systemUrl = `${baseUrl}`;
const formName = 'Betaling internetkassa Ingenico automatische test';
const BSN = '010082426';
let caseNumber = '0000000';
const paymentResults = ['EXCEPTION', 'FAILURE', 'SUCCESS', 'CANCELLED'];
const paymentTests = [
  {
    paymentResult: 'EXCEPTION',
    reason: 'Ik betaal dit bedrag NIET om een EXCEPTION tijdens het betalen te testen',
    amount: '381,00',
    resultHeaderText: 'Uw betaling is mislukt.',
    resultText1: ' voor meer informatie.Gebruik zaaknummer ',
    resultText2: ' als referentie.',
    resultText3: '',
  },
  {
    paymentResult: 'CANCELLED',
    reason: 'Ik betaal dit bedrag NIET om het CANCELLED zijn van een betaling te testen',
    amount: '119,00',
    resultHeaderText: 'Uw betaling is geannuleerd.',
    resultText1: 'De betaling voor uw aanvraag is door u geannuleerd, de aanvraag wordt niet in behandeling genomen en is vernietigd.',
    resultText2: '',
    resultText3: '',
  },
  {
    paymentResult: 'FAILURE',
    reason: 'Ik betaal dit bedrag NIET om FAILURE van de betaling te testen',
    amount: '541,00',
    resultHeaderText: 'Uw betaling is geannuleerd.',
    resultText1: 'De betaling voor uw aanvraag is door u geannuleerd, de aanvraag wordt niet in behandeling genomen en is vernietigd.',
    resultText2: '',
    resultText3: '',
  },
  {
    paymentResult: 'SUCCESS',
    reason: 'Ik betaal dit bedrag om SUCCESS van de betaling te testen',
    amount: '888,00',
    resultHeaderText: 'Uw betaling is gelukt',
    resultText1: 'Bedankt voor het aangaan van een Betaling internetkassa Ingenico automatische test.' +
      ' Uw registratie is bij ons bekend onder zaaknummer ',
    resultText2: '. Wij verzoeken u om bij verdere communicatie ' +
      'dit zaaknummer te gebruiken. De behandeling van deze zaak zal spoedig plaatsvinden.',
    resultText3: 'Ook kunt u op elk moment van de dag de voortgang en inhoud inzien via de persoonlijke internetpagina',
  },
];

async function logbookSearchCase(page: Page) {
  await page.getByPlaceholder('Zaaknummer').click();
  await page.getByPlaceholder('Zaaknummer').type(`${caseNumber.padStart(3, '0')}`);
  await page.waitForResponse(`${baseUrl}api/v1/eventlog?page=1&rows_per_page=50&query:match:case_id=${caseNumber.padStart(3, '0')}`,
    { timeout: extreemLongTimeout });
  await page.waitForLoadState('domcontentloaded');
  await page.waitForLoadState('networkidle');
  await expect(page.getByText('Component')).toBeVisible({ timeout: extreemLongTimeout });
}

paymentResults.forEach(function x(paymentResultInTest) {
  const { paymentResult, reason, amount, resultText1, resultText2, resultText3, resultHeaderText,
  } = paymentTests.find(paymentTest => paymentTest.paymentResult === paymentResultInTest) ?? paymentTests[0];
  test.describe.serial(
    `Person pays through form and Digid login. Employee treats case. Payment result: ${paymentResultInTest}`, () => {
      let page: Page;
      let context: BrowserContext;

      test.beforeAll(async ({ browser }, testInfo) => {
        ({ context, page } = await allTestsBeforeAll(context, browser, page, testInfo));
      });

      test.afterAll(async () => {
        await context.close();
      });

      test(`Log person in for test ${paymentResultInTest} @inStartE2E`, async ({ }, testInfo) => {
        await personFormDigidLogin({
          page,
          formName,
          BSN,
          context,
          testTitle: testInfo.title
        });
      });

      test(`Check person data for test ${paymentResultInTest} @inStartE2E`, async ({ }, testInfo) => {
        await checkPersonDataInForm({ page, testTitle: testInfo.title, BSN });
      });

      test(`Go to the first form page for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
        await page.getByRole('button', { name: 'Volgende' }).click({ delay: clickDelay });
      });

      test(`Check if form was saved, if so continue for test nr ${paymentResultInTest} @inStartE2E`, async ({ }, testInfo) => {
        try {
          await expect(page.getByText('Nee, ik wil alle gegevens opnieuw invullen')).toBeVisible({ timeout: shortTimeout });
          await page.getByRole('button', { name: 'Volgende' }).click({ delay: clickDelay });
          await page.waitForLoadState('domcontentloaded');
          await page.waitForLoadState('networkidle');
        }
        catch {
          await expect.poll(async () => {
            try {
              await expect(page.getByRole('heading', { name: 'Uitleg test' })).toBeVisible({ timeout: shortTimeout });
            }
            catch {
              console.log(`Need to reload for test ${paymentResultInTest}`);
              await page.reload({ waitUntil: 'domcontentloaded' });
              await page.waitForLoadState('networkidle');
              try {
                await expect(page.getByRole('link', { name: 'Inloggen met SAML IdP: DigiD development' }))
                  .toBeHidden({ timeout: shortTimeout });
              }
              catch {
                await retrySpoofUntilSucceeded(page, BSN, context, testInfo.title);
              }
              await page.getByRole('button', { name: 'Volgende' }).scrollIntoViewIfNeeded();
              await page.getByRole('button', { name: 'Volgende' }).click({ delay: clickDelay });
            }
            return page.getByRole('heading', { name: 'Uitleg test' }).isVisible();
          }, {
            // Probe, wait 1s, probe, wait 2s, probe, wait 5s, probe, wait 5s, probe, .... Defaults to [100, 250, 500, 1000].
            // intervals: [1_000, 2_000, 5_000],
            timeout: 60_000
          }).toBeTruthy();
        }
      });

      test(`Check payment explanation page for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
        await expect(page.getByRole('heading', { name: 'Uitleg test' })).toBeVisible();
        await expect(page.getByText('Dit formulier dient om de ogone/inigenico betalings koppeling te testen')).toBeVisible();
        await expect(page.getByText('Dit is een test voor de Ogone betaling')).toBeVisible();
      });

      test(`Go to the payment page for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
        await page.getByRole('button', { name: 'Volgende' }).click();
        await page.waitForLoadState('domcontentloaded');
        await page.waitForLoadState('networkidle');
      });

      test(`Fill ZTC bedrag for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
        await page.getByRole('textbox').click();
        await page.getByRole('textbox').fill(`${amount}`);
      });

      test(`Fill reason of paying for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
        await page.locator('//*[@class="ql-editor ql-blank"]').first().click();
        await page.locator('//*[@class="ql-editor ql-blank"]').first().press('Control+b');
        await page.locator('//*[@class="ql-editor ql-blank"]').first().fill(reason);
      });

      test(`Go to the overview page for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
        await page.getByRole('button', { name: 'Volgende' }).click();
        await page.waitForLoadState('domcontentloaded');
        await page.waitForLoadState('networkidle');
        try {
          await expect(page.getByRole('heading', { name: 'Controleer uw invoer en overige acties' })).toBeVisible();
        }
        catch {
          await page.getByRole('button', { name: 'Volgende' }).click();
          await page.waitForLoadState('domcontentloaded');
          await page.waitForLoadState('networkidle');
        }
      });

      test(`Check the reason on the overview page for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
        await page.getByText('Reden van donatie').scrollIntoViewIfNeeded();
        await expect(page.locator(locators.labelLeftOnPersonFormOverview('Reden van donatie', '2')).first()).toHaveText(`${reason}`);
      });

      test(`Check the amount on the overview page for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
        await page.getByText('Donatie bedrag', { exact: true }).scrollIntoViewIfNeeded();
        try {
          await expect(page.locator(locators.labelLeftOnPersonFormOverview('Donatie bedrag', '2')).first()).toHaveText(`${amount}`);
        }
        catch {
          await expect(page.locator(locators.labelLeftOnPersonFormOverview('Donatie bedrag', '2')).first())
            .toHaveText(`${amount.replace(',', '.')}`);
        }
      });

      test(`Sent the form for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
        await page.getByRole('button', { name: 'Versturen' }).click();
        await page.waitForLoadState('domcontentloaded');
        await page.waitForLoadState('networkidle');
      });

      test(`Save the case number for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
        caseNumber = await page.locator(locators.labeledField('Zaaknummer')).innerText({ timeout: superLongTimeout });
      });

      test(`Check the case made data for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
        await expect(page.locator(locators.labeledField('Product of dienst'))).toHaveText(`${formName}`);
        await page.locator(locators.labeledField('Bedrag')).scrollIntoViewIfNeeded();
        const amountFound = await page.locator(locators.labeledField('Bedrag')).innerText();
        expect(amountFound === `€ ${amount}` || amountFound === `€ ${amount.replace(',', '.')}`).toBeTruthy();
      });

      test(`Start paying for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
        await page.getByRole('button', { name: 'Betalen' }).first().click({ timeout: longTimeout });
        await page.getByRole('button', { name: 'iDEAL' }).click();
        await page.getByRole('combobox', { name: 'Selecteer uw bank' }).selectOption('9999+TST');
        await page.getByRole('button', { name: 'Ga verder' }).click();
      });

      test(`Choose result ${paymentResultInTest} @inStartE2E`, async ({ }) => {
        await page.getByRole('button', { name: `${paymentResult}` }).click({ timeout: longTimeout });
      });

      test(`Check result in Ogone for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
        // eslint-disable-next-line playwright/no-conditional-in-test
        if (paymentResult === 'FAILURE' || paymentResult === 'CANCELLED') {
          page.once('dialog', dialog => dialog.accept());
          await page.getByRole('button', { name: 'Annuleren' }).click({ delay: clickDelay });
        }
        await page.getByRole('button', { name: 'OK' }).click();
      });

      test(`Check result in zs.nl for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
        await expect(page.getByRole('heading', { name: `${resultHeaderText}` })).toBeVisible();
        // eslint-disable-next-line playwright/no-conditional-in-test
        paymentResult === 'FAILURE' || paymentResult === 'CANCELLED' ?
          await expect(page.getByText(`${resultText1}`)).toBeVisible()
          :
          await expect(page.getByText(`${resultText1}${caseNumber}${resultText2}`)).toBeVisible();
        // eslint-disable-next-line playwright/no-conditional-in-test
        resultText3 !== '' ? await expect(page.getByText(`${resultText3}`)).toBeVisible() : '';
      });

      test(`Go to person webpage for test ${paymentResultInTest} @inStartE2E`, async ({ }, testInfo) => {
        // eslint-disable-next-line playwright/no-conditional-in-test
        paymentResult === 'SUCCESS' ?
          await page.getByRole('link', { name: 'Mijn ' }).click({ timeout: longTimeout })
          :
          // eslint-disable-next-line playwright/no-conditional-in-test
          paymentResult === 'EXCEPTION' ?
            await page.goto(`${baseUrl}\pip`)
            :
            await personPipDigidLogin({
              page,
              BSN,
              context,
              testTitle: testInfo.title
            });
      });

      if (paymentResult === 'FAILURE' || paymentResult === 'CANCELLED') {
        test(`Check case not present ${paymentResultInTest} @inStartE2E`, async ({ }) => {
          await expect(page.getByText(`${caseNumber}: ${formName}`)).toBeHidden();
        });
      }
      else {
        test(`Check case present ${paymentResultInTest} @inStartE2E`, async ({ }) => {
          await checkTextReload({ page, textToToCheck: `${caseNumber}: ${formName}`, visible: true });
        });

        test(`Open case for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
          await page.getByRole('link',
            { name: `${caseNumber}: Betaling internetkassa Ingenico automatische test` }).click({ timeout: superLongTimeout });
        });

        test(`Open the Registration fase for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
          await page.getByText('Registreren').click();
        });

        test(`Check the case details for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
          await page.getByText('Reden van donatie').scrollIntoViewIfNeeded();
          await expect(page.getByText(`${reason}`)).toBeVisible();
          await expect(page.getByText(`${amount.replace(',', '.')}`)).toBeVisible();
        });
      }

      //Test turned because of not reachable menu after layout changes
      // test(`Log off person for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
      //   await pipLogOff({ page });
      // });

      test(`Log on as employee for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
        await openSystemAndLogin({
          page,
          systemUrl,
          user: beheerderUser
        });
      });

      test(`Go to Logboek first time for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
        await navigateAdministration({ page, menuItem: 'Logboek' });
      });

      test(`Search for the casenumber for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
        await logbookSearchCase(page);
      });

      if (paymentResult === 'FAILURE' || paymentResult === 'CANCELLED') {
        test(`Check case deleted logging for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
          await expect(page.getByText(`Zaak ${caseNumber} vernietigd door Ogone`)).toBeVisible({ timeout: longTimeout });
        });

        test(`Open deleted case for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
          await page.getByRole('link', { name: `${caseNumber}` }).click();
        });

        test(`Check error message deleted case for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
          await expect(page.getByText('Er ging iets mis bij het laden van de zaak. ' +
            'Neem contact op met uw beheerder voor meer informatie')).toBeVisible();
          await page.getByRole('link', { name: 'Terug naar dashboard' }).click();
        });
      }
      else {
        test(`Check case is registered for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
          await expect(page.getByText(`Zaak ${caseNumber} geregistreerd`)).toBeVisible({ timeout: longTimeout });
        });

        test(`Open the case for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
          await page.getByRole('row', { name: 'row' })
            .filter({ hasText: `Zaak ${caseNumber} geregistreerdZaak-` }).getByRole('link', { name: `${caseNumber}` }).click();
        });

        test(`Open phase registration for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
          await page.getByRole('link', { name: 'Afgeronde fase Registreren' }).click();
          await page.waitForLoadState('domcontentloaded');
          await page.waitForLoadState('networkidle');
        });

        test(`Check reason in phase registration for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
          await page.locator(locators.labelLeftRichTextFatt('Reden van donatie')).scrollIntoViewIfNeeded({ timeout: longTimeout });
          await expect(page.locator(locators.labelLeftRichTextFatt('Reden van donatie'))).toHaveText(reason);
        });

        test(`Check amount in phase registration for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
          await page.getByLabel('Donatie bedrag*').scrollIntoViewIfNeeded();
          const amountFound = await page.getByLabel('Donatie bedrag*').inputValue();
          expect(amountFound).toEqual(amount);
        });

        test(`Open information panel left for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
          const panelIsNotOpen = await page.locator('//zs-icon[@icon-type="chevron-right"]').nth(1).isVisible();
          // eslint-disable-next-line playwright/no-conditional-in-test
          panelIsNotOpen ? await page.getByRole('button', { name: '' }).click() : '';
        });

        test(`Check amount in information panel left for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
          // eslint-disable-next-line playwright/no-conditional-in-test
          const amountFound = paymentResult === 'SUCCESS' ?
            await page.getByRole('listitem', { name: 'Betaalstatus: Geslaagd' }).locator('a').innerText()
            :
            await page.getByRole('listitem', { name: 'Betaalstatus: Wachten op bevestiging' }).locator('a').innerText();
          expect(amountFound).toEqual(amount);
        });

        test(`Check status symbol and information message for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
          // eslint-disable-next-line playwright/no-conditional-in-test
          if (paymentResult === 'SUCCESS') {
            await page.getByRole('listitem', { name: 'Betaalstatus: Geslaagd' }).locator('a').hover();
            await expect(page.getByText('Betaalstatus: Geslaagd')).toBeVisible();
          }
          else {
            await page.getByRole('listitem', { name: 'Betaalstatus: Wachten op bevestiging' }).locator('a').hover();
            await expect(page.getByText('Betaalstatus: Wachten op bevestiging')).toBeVisible();
          }
        });

        test(`Check payment status in information panel left for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
          // eslint-disable-next-line playwright/no-conditional-in-test
          paymentResult === 'SUCCESS' ?
            await expect(page.getByRole('listitem', { name: 'Betaalstatus: Geslaagd' }).locator('a')).toBeVisible()
            :
            await expect(page.getByRole('listitem', { name: 'Betaalstatus: Wachten op bevestiging' }).locator('a')).toBeVisible();
        });

        test(`Close left panel again for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
          const panelIsOpen = await page.locator('//zs-icon[@icon-type="chevron-left"]').nth(0).isVisible();
          // eslint-disable-next-line playwright/no-conditional-in-test
          panelIsOpen ? await page.getByRole('button', { name: '' }).nth(0).click() : '';
        });

        test(`Finish case treatment for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
          await page.getByRole('link', { name: 'Huidige fase Afhandelen' }).click();
          await page.getByLabel('Afgehandeld').first().check();
          await page.getByRole('listitem')
            .filter({ hasText: 'Deze zaak heeft nog geen behandelaar. In behandeling nemen' })
            .getByRole('button', { name: 'In behandeling nemen' }).click();
          await page.getByRole('button', { name: ' Zaak afhandelen' }).click();
        });

        test(`Go to Logboek second time for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
          await navigateAdministration({ page, menuItem: 'Logboek' });
        });

        test(`Search second time for the casenumber for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
          await logbookSearchCase(page);
        });

        test(`Check case treated for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
          try {
            await expect(page.getByText(`Zaak ${caseNumber} afgehandeld: Afgehandeld (afgehandeld)`)).toBeVisible({ timeout: longTimeout });
          }
          catch {
            await expect(page.getByText(`Zaak ${caseNumber} afgehandeld: Afgehandeld (afgehandeld)`)).toBeVisible({ timeout: longTimeout });
          }
        });
      }

      test(`Logoff employee for test ${paymentResultInTest} @inStartE2E`, async ({ }) => {
        await logOff({ page, systemUrl });
      });
    });

});

