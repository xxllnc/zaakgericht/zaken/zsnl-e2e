import { BrowserContext, expect, Page, test } from '@playwright/test';
import { baseUrl, beheerderUser } from '../../e2e_tests';
import {
  logOff, openSystemAndLogin, checkOrAddCategoryOrFolder
} from '../../partial-tests';
import { zsnlTestConfig } from '../../playwright.config';
import { allTestsBeforeAll, navigateAdministration } from '../../utils';

const { typeDelay, superLongTimeout } = zsnlTestConfig;
const systemUrl = `${baseUrl}`;
const casetype = 'Basiszaaktype dun automatische test';
const categoryOrFolderArray = [
  { categoryOrFolder: 'ZTC zaaktypen', selectAllText: 'zaaktype', childNumber: '0' },
  { categoryOrFolder: 'ZTC kenmerken', selectAllText: 'kenmerk', childNumber: '6' },
  { categoryOrFolder: 'ZTC notificaties', selectAllText: 'bericht', childNumber: '7' },
  { categoryOrFolder: 'ZTC sjablonen', selectAllText: 'sjabloon', childNumber: '8' },
];

test.describe.serial(`Import ${casetype}`, () => {
  let page: Page;
  let context: BrowserContext;

  test.beforeAll(async ({ browser }, testInfo) => {
    ({ context, page } = await allTestsBeforeAll(context, browser, page, testInfo));

    await openSystemAndLogin({
      page,
      systemUrl,
      user: beheerderUser
    });
  });

  test.afterAll(async () => {
    await logOff({ page, systemUrl });
    await context.close();
  });

  test('Open admin section @fill_empty_db', async ({ }) => {
    await navigateAdministration({ page, menuItem: 'Catalogus' });
  });

  Object.values(categoryOrFolderArray).forEach(pair => {
    test(`Check and if not present make category ${pair.categoryOrFolder} @fill_empty_db`, async ({ }) => {
      await checkOrAddCategoryOrFolder({ page, categoryOrFolder: `${pair.categoryOrFolder}` });
    });
  });

  test('Start import @fill_empty_db', async ({ }) => {
    await page.getByTestId('AddIcon').nth(1).click();
    await page.getByRole('button', { name: 'Importeren' })
      .click();
  });

  test('Select Zaaktypen @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Zaaktypen')
      .check();
  });

  test('Select Zaaksysteem.nl @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Zaaksysteem.nl')
      .check();
  });

  test('Add case type file @fill_empty_db', async () => {
    const fileChooserPromise = page.waitForEvent('filechooser');
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Kies bestand' })
      .click();
    const fileChooser = await fileChooserPromise;
    await fileChooser.setFiles('testfiles/Basiszaaktype_dun_automatische_test.ztb');
  });

  test('Wait for upload then click volgende @fill_empty_db', async () => {
    await expect(page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByText('Basiszaaktype_dun_automatische_test.ztb')).toBeVisible();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Volgende' })
      .click();
  });

  test('Expect Zaaktypen to be visible @fill_empty_db', async ({ }) => {
    await expect(page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('heading',
      { name: 'Zaaktypen' }))
      .toBeVisible();
  });

  Object.values(categoryOrFolderArray).forEach(pair => {
    test(`Start the select category/folder for the first ${pair.categoryOrFolder} @fill_empty_db`, async ({ }) => {
      // eslint-disable-next-line playwright/no-conditional-in-test
      if (pair.childNumber === '0') {
        await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('.toggle').first()
          .click();
      }
      else {
        await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator(
          `div:nth-child(${pair.childNumber}) > .ezra_import_dependency_group_inner > .import_group_inner >` +
          ' div:nth-child(2) > .import_dependency_status > .title > .toggle')
          .click();
      }
    });

    test(`Select Voeg meegeleverde ${pair.categoryOrFolder} toe @fill_empty_db`, async ({ }) => {
      await page.frameLocator('internal:attr=[title="Beheer formulier"i]')
        .getByRole('strong').filter({ hasText: `${pair.selectAllText} toe` }).locator('span')
        .click();
    });

    test(`Select this choice for all ${pair.categoryOrFolder} @fill_empty_db`, async ({ }) => {
      await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByText(
        `Geselecteerde categorie voor alle nieuwe ${pair.selectAllText} gebruiken`
      ).click();
    });

    test(`Select category / folder for the ${pair.categoryOrFolder} @fill_empty_db`, async ({ }) => {
      const locie = page.frameLocator('internal:attr=[title="Beheer formulier"i]')
        .locator(`//select/option[contains(.//text(),"- ${pair.categoryOrFolder}")]`).first();
      // eslint-disable-next-line playwright/no-conditional-in-test
      const optionNumber = await locie.getAttribute('value') || -1;
      await page.frameLocator('internal:attr=[title="Beheer formulier"i]')
        .locator('select[name="bibliotheek_categorie_id"]')
        .selectOption(optionNumber.toString());
    });

    test(`Save category/folder for all ${pair.categoryOrFolder} @fill_empty_db`, async ({ }) => {
      await page.frameLocator('iframe[title="Beheer formulier"]').locator('button[name="action"]').click();
      await expect(page.frameLocator('internal:attr=[title="Beheer formulier"i]')
        .getByRole('strong').filter({ hasText: `${pair.selectAllText} toe` }).locator('span'))
        .toBeHidden({ timeout: superLongTimeout });
    });
  });

  test('Import casetype @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]')
      .getByRole('button', { name: 'Importeren' })
      .click();
    await expect(page.frameLocator('iframe[title="Beheer formulier"]').getByText(`Zaaktype '${casetype}' is geïmporteerd.`)).toBeVisible({ timeout: superLongTimeout });
  });

  test('Return to Catalogus @fill_empty_db', async ({ }) => {
    await page.getByRole('link', { name: 'Catalogus Catalogus' }).click();
  });

  test('Check adding of casetype @fill_empty_db', async ({ }) => {
    await page.getByPlaceholder('Zoeken in de catalogus').fill(casetype);
    await page.getByPlaceholder('Zoeken in de catalogus').press('Enter', { delay: typeDelay });
    const count = await page.getByText(casetype).count();
    expect(count).toBeGreaterThanOrEqual(1);
  });

});