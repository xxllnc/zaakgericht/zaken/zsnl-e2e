import { BrowserContext, expect, Page, test } from '@playwright/test';
import { baseUrl, beheerderUser } from '../../e2e_tests';
import { logOff, openSystemAndLogin } from '../../partial-tests';
import { zsnlTestConfig } from '../../playwright.config';
import { allTestsBeforeAll, locators, navigateAdministration } from '../../utils';
import { employeeData } from '../../testfiles/employeeData';

const { longTimeout, shortTimeout } = zsnlTestConfig;
const employeesToAdd = ['autoadmin', 'autobeheerder', 'autobehandelaar'];
const newDepartment = 'Automatiseren';
let employeeFound = false;
let departmentFound = false;

test.describe.serial('Adding department and/or employees', () => {
  let page: Page;
  let context: BrowserContext;

  test.beforeAll(async ({ browser }, testInfo) => {
    ({ context, page } = await allTestsBeforeAll(context, browser, page, testInfo));
    await openSystemAndLogin({
      page,
      systemUrl: baseUrl,
      user: beheerderUser
    });
  });

  test.afterAll(async () => {
    await logOff({ page, systemUrl: baseUrl });
    await context.close();
  });

  test('Go to Gebruikers', async ({ }) => {
    await expect(page.locator(locators.partialTextLocator('Mijn openstaande zaken'))).toBeVisible({ timeout: longTimeout });

    await navigateAdministration({ page, menuItem: 'Gebruikers' });
  });

  test('Open the main department', async ({ }) => {
    // eslint-disable-next-line playwright/no-conditional-in-test
    await page.frameLocator('iframe[title="Beheer formulier"]')
      .getByRole('row', {
        name: /^(Development |GBT |HoofdOrganisatie )$/
      }).locator('span').first().click();
    await expect(page.frameLocator('iframe[title="Beheer formulier"]')
      .getByRole('row', { name: 'Backoffice' }).locator('span').first()).toBeVisible();
  });

  test('Check and if not present, add new department', async ({ }, testInfo) => {
    await expect(page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByText(newDepartment).nth(0))
      .toBeVisible({ timeout: shortTimeout })
      .then(() => {
        departmentFound = true;
      }).catch(e => {
        console.log(`${testInfo.title} The error ${e} was given, when searching for the deparment`);
        departmentFound = false;
      });
  });

  test('Add department', async ({ }) => {
    // eslint-disable-next-line playwright/no-skipped-test
    test.skip(departmentFound, 'The department is already there');

    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('link', { name: ' ' }).first().click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('link', { name: 'Afdeling' }).first().click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('textbox').fill(newDepartment);
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Opslaan' }).click();
    await page.waitForRequest(`${baseUrl}api/v1/session/current`);
  });

  test('Open again the main department', async ({ }) => {
    // eslint-disable-next-line playwright/no-skipped-test
    test.skip(departmentFound, 'The department is already there');

    // reload added ivm disconnected screen
    await page.reload({ waitUntil: 'domcontentloaded' });
    await page.frameLocator('iframe[title="Beheer formulier"]')
      .getByRole('row', {
        name: /^(Development |GBT |HoofdOrganisatie )$/
      }).locator('span').first().click();
    await page.waitForLoadState('domcontentloaded');
    await page.waitForLoadState('networkidle');
    await expect(page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByText('Backoffice').nth(0)).toBeVisible({ timeout: longTimeout });
  });

  test('Open the department', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByText(newDepartment).nth(0).click();
    await page.waitForLoadState('domcontentloaded');
    await page.waitForLoadState('networkidle');
  });

  employeesToAdd.forEach(function x(employeeInTest) {
    const { username, initials, firstName, lastName, emailAddress, userFunction, password, displayName,
      roles } = employeeData.find(employee => employee.username === employeeInTest) ?? employeeData[0];

    test(`Check if employee ${employeeInTest} is present`, async ({ }, testInfo) => {
      await expect(page.frameLocator('internal:attr=[title="Beheer formulier"i]')
        .getByText(`${employeeInTest}`).nth(0)).toBeVisible({ timeout: shortTimeout })
        .then(() => {
          employeeFound = true;
        }).catch(e => {
          console.log(`${testInfo.title}: When searching for ${employeeInTest} the error was given: ${e}`);
          employeeFound = false;
        });
    });

    test(`Start adding employee ${employeeInTest}`, async ({ }) => {
      // eslint-disable-next-line playwright/no-skipped-test
      test.skip(employeeFound, `Employee ${employeeInTest} is already there`);

      await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('link', { name: ' ' }).first().click();
      await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('link', { name: 'Medewerker' }).click();
    });

    test(`Fill employee data ${employeeInTest}`, async ({ }) => {
      // eslint-disable-next-line playwright/no-skipped-test
      test.skip(employeeFound, `Employee ${employeeInTest} is already there`);

      await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="initials"]').fill(`${initials}`);
      await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="givenname"]').fill(`${firstName}`);
      await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="sn"]').fill(`${lastName}`);
      await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="displayname"]').fill(`${displayName}`);
      await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="mail"]').fill(`${emailAddress}`);
      await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="title"]').fill(`${userFunction}`);
      const locie = page.frameLocator('internal:attr=[title="Beheer formulier"i]')
        .locator('//select/option[contains(.//text(),"Automatiseren")]');
      // eslint-disable-next-line playwright/no-conditional-in-test
      const optionNumber = await locie.getAttribute('value') || -1;
      await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('combobox').selectOption(optionNumber.toString());
      await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="username"]').fill(`${username}`);
      await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="username"]').press('Tab');
      // eslint-disable-next-line playwright/no-conditional-in-test
      const usePassword = baseUrl.indexOf('dev.') === 8 ?
        `${password}` : `${beheerderUser.password}`;
      await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="password"]').click();
      await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="password"]')
        .fill(usePassword);
      await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="password_confirmation"]').click();
      await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="password_confirmation"]')
        .fill(usePassword);
    });

    test(`Save the new employee ${employeeInTest}`, async ({ }) => {
      // eslint-disable-next-line playwright/no-skipped-test
      test.skip(employeeFound, `Employee ${employeeInTest} is already there`);
      await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Opslaan' }).click();
      await expect(page.frameLocator('iframe[title="Beheer formulier"]')
        .getByRole('row', { name: `${newDepartment}` }).locator('span').first()).toBeHidden();
      await page.waitForLoadState('domcontentloaded');
      await page.waitForLoadState('networkidle');
    });

    test(`Open again the department for ${employeeInTest}`, async ({ }) => {
      // eslint-disable-next-line playwright/no-skipped-test
      test.skip(employeeFound, `Employee ${employeeInTest} is already there`);

      // eslint-disable-next-line playwright/no-conditional-in-test
      await page.frameLocator('iframe[title="Beheer formulier"]')
        .getByRole('row', {
          name: /^(Development |GBT |HoofdOrganisatie )$/
        }).locator('span').first().click();
      await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByText(newDepartment).nth(0).click();
      await page.waitForLoadState('domcontentloaded');
      await page.waitForLoadState('networkidle');
    });

    roles.forEach(role => {
      test(`Add the role ${role} for ${employeeInTest}`, async ({ }) => {
        // eslint-disable-next-line playwright/no-skipped-test
        test.skip(employeeFound, `Employee ${employeeInTest} is already there`);
        const countRole = await page.getByText(`${role}`).count();
        await page.frameLocator('iframe[title="Beheer formulier"]').getByText(`${role}`).nth(countRole - 1).hover();
        await page.mouse.down();
        await page.frameLocator('iframe[title="Beheer formulier"]').getByText(`${employeeInTest}`).hover();
        await page.mouse.up();
        await page.frameLocator('iframe[title="Beheer formulier"]').getByText(`${displayName}`).nth(0).hover();
      });

      test(`Check message about the role ${role} for ${employeeInTest}`, async ({ }) => {
        // eslint-disable-next-line playwright/no-skipped-test
        test.skip(employeeFound, `Employee ${employeeInTest} is already there`);
        await expect(page.frameLocator('iframe[title="Beheer formulier"]')
          .getByText(`Rol "${role}" toegevoegd aan "${displayName}"`)).toBeVisible({ timeout: shortTimeout });
      });

      test(`Close message about the role ${role} for ${employeeInTest}`, async ({ }) => {
        // eslint-disable-next-line playwright/no-skipped-test
        test.skip(employeeFound, `Employee ${employeeInTest} is already there`);
        await page.frameLocator('iframe[title="Beheer formulier"]').getByRole('button', { name: 'x' }).click();
      });
    });
  });
});

