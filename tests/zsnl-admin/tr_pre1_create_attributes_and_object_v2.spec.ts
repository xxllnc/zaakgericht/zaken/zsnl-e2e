import { BrowserContext, Page, test } from '@playwright/test';
import { baseUrl, beheerderUser } from '../../e2e_tests';
import {
  checkOrAddCategoryOrFolder,
  logOff, openSystemAndLogin
} from '../../partial-tests';
import { zsnlTestConfig } from '../../playwright.config';
import { checkOrAddAttribute } from '../../partial-tests/checkOrAddAttribute';
import { navigateAdministration } from '../../utils';
import { testrondeDataArray, testrondeObjectTypeV2 } from '../../testfiles/TESTRONDE/testrondeTestdata';

const { options } = zsnlTestConfig;
const categoryAndFolder = 'Automatische E2E TESTRONDE';

test.describe.serial('TR PRE1 Create attributes and objecttype for E2E TESTRONDE', () => {
  async function addRoleAndRights(page: Page, department: string, roleNr: number) {
    await page.getByRole('button', { name: 'Rol en afdeling toevoegen' }).click();
    await page.getByPlaceholder('Selecteer een afdeling').nth(roleNr).fill(department);
    await page.getByText(department).last().click();
    await page.getByPlaceholder('Selecteer een rol').nth(roleNr).click();
    await page.getByPlaceholder('Selecteer een rol').nth(roleNr).fill('behand');
    await page.getByRole('option', { name: /Behandelaar/ }).click();
    await page.getByLabel('Mag beheren').nth(roleNr).check();
  }

  let page: Page;
  let context: BrowserContext;

  test.beforeAll(async ({ browser }) => {
    context = await browser.newContext(options);
    page = await context.newPage();
    await openSystemAndLogin({
      page,
      systemUrl: baseUrl,
      user: beheerderUser
    });
  });

  test.afterAll(async () => {
    await logOff({ page, systemUrl: baseUrl });
    await context.close();
  });

  test('Open admin section @fill_empty_db', async ({ }) => {
    await navigateAdministration({ page, menuItem: 'Catalogus' });
  });

  test(`Check and if not present make categoryAndFolder ${categoryAndFolder} @fill_empty_db`, async ({ }) => {
    await checkOrAddCategoryOrFolder({ page, categoryOrFolder: `${categoryAndFolder}` });
  });

  Object.values(testrondeDataArray).filter(
    attribute => attribute.magicString !== 'auto_relatie_kenmerk_testronde_v2_object'
  ).forEach(attribute => {
    //All the attributes for TESTRONDE are being made here
    test(`Check and if not present make attribute ${attribute.nameAttribute} @fill_empty_db`, async ({ }) => {
      await checkOrAddAttribute({
        page,
        ...attribute,
        desiredFolderName: `${categoryAndFolder}`,
      });
    });
  });

  test('Go to the Auto objects folder @fill_empty_db', async ({ }) => {
    await page.getByPlaceholder('Zoeken in de catalogus…').click();
    await page.getByPlaceholder('Zoeken in de catalogus…').fill(`${categoryAndFolder}`);
    await page.getByPlaceholder('Zoeken in de catalogus…').press('Enter');
    await page.getByRole('link', { name: `${categoryAndFolder}` }).click();
  });

  test('Start creating the object @fill_empty_db', async ({ }) => {
    await page.getByTestId('AddIcon').nth(1).click();
    await page.getByRole('button', { name: 'Objecttype v2' }).click();
  });

  test('Add object name @fill_empty_db', async ({ }) => {
    await page.getByPlaceholder('Geef een herkenbare naam voor het objecttype').click();
    await page.getByPlaceholder('Geef een herkenbare naam voor het objecttype').fill(testrondeObjectTypeV2);
  });

  test('Add object title @fill_empty_db', async ({ }) => {
    await page.getByPlaceholder('Geef een titel voor de objecten van dit objecttype').click();
    await page.getByPlaceholder('Geef een titel voor de objecten van dit objecttype')
      .fill('Auto Testronde [[ j("$.attributes.custom_fields.auto_tst_k1000_datum_release_tag") ]] -' +
        ' [[ j("$.attributes.custom_fields.auto_tst_o0001_object_naam") ]]');
  });

  test('Add object subtitle @fill_empty_db', async ({ }) => {
    await page.getByPlaceholder('Geef een subtitel voor de objecten van dit objecttype').click();
    await page.getByPlaceholder('Geef een subtitel voor de objecten van dit objecttype')
      .fill('Object V2 Aangemaakt en gebruikt in de automatische test');
  });

  test('Add object external source @fill_empty_db', async ({ }) => {
    await page.getByPlaceholder('Referentie voor bron buiten Zaaksysteem.nl').click();
    await page.getByPlaceholder('Referentie voor bron buiten Zaaksysteem.nl').fill('Playwright');
  });

  test('Go to the Attributes page @fill_empty_db', async ({ }) => {
    await page.getByRole('button', { name: 'Volgende' }).click();
  });

  Object.values(testrondeDataArray).filter(
    attribute => attribute.inObject)
    .forEach(attribute => {
      test(`Add the attribute ${attribute.nameAttribute} to the object @fill_empty_db`, async ({ }) => {

        await page.getByPlaceholder('Zoek en voeg toe').click();
        await page.getByPlaceholder('Zoek en voeg toe').fill(`${attribute.nameAttribute}`);
        await page.getByRole('option', { name: `${attribute.nameAttribute}`, exact: true }).first().click();
      });

      test(`Set the attribute ${attribute.nameAttribute} title @fill_empty_db`, async ({ }) => {
        await page.getByRole('button', { name: 'Instellen', exact: true }).last().click();
        await page.getByRole('textbox', { name: 'Titel' }).click();
        await page.getByRole('textbox', { name: 'Titel' }).fill(`${attribute.title}`);
      });

      test(`Set the attribute ${attribute.nameAttribute} explanation (external) @fill_empty_db`, async ({ }) => {
        await page.locator('//textarea[@name="external_description"]').click();
        await page.locator('//textarea[@name="external_description"]').fill(`${attribute.explanationExternal}`);
      });

      test(`Set the attribute ${attribute.nameAttribute} mandatory @fill_empty_db`, async ({ }) => {
        // eslint-disable-next-line playwright/no-conditional-in-test
        let mandatory = false;
        for (let index = 0; index < Object.entries(attribute).length; index++) {
          const keyProperty = Object.keys(attribute)[index];
          // eslint-disable-next-line playwright/no-conditional-in-test
          if (keyProperty === 'mandatory') {
            mandatory = Object.values(attribute)[index] === true;
            break;
          }
        }
        // eslint-disable-next-line playwright/no-conditional-in-test
        mandatory ? await page.getByLabel('Verplicht veld').check() : '';
      });

      test(`Set the attribute ${attribute.nameAttribute} system attribute @fill_empty_db`, async ({ }) => {
        // eslint-disable-next-line playwright/no-conditional-in-test
        let systemAttribute = false;
        for (let index = 0; index < Object.entries(attribute).length; index++) {
          const keyProperty = Object.keys(attribute)[index];
          // eslint-disable-next-line playwright/no-conditional-in-test
          if (keyProperty === 'systemAttribute') {
            systemAttribute = Object.values(attribute)[index] === true;
            break;
          }
        }
        // eslint-disable-next-line playwright/no-conditional-in-test
        systemAttribute ? await page.getByLabel('Gebruik als systeemkenmerk').check() : '';
      });

      test(`Save the attribute ${attribute.nameAttribute} @fill_empty_db`, async ({ }) => {
        await page.getByRole('button', { name: 'Opslaan' }).click();
      });
    });

  test('Go to the Relations page @fill_empty_db', async ({ }) => {
    await page.getByRole('button', { name: 'Volgende' }).click();
  });

  test('Go to the Rights page @fill_empty_db', async ({ }) => {
    await page.getByRole('button', { name: 'Volgende' }).click();
  });

  test('Add the role and rights for AUTO behandelaar @fill_empty_db', async ({ }) => {
    await addRoleAndRights(page, 'Automatiseren', 0);
  });

  test('Add the role and rights for FO behandelaar @fill_empty_db', async ({ }) => {
    await addRoleAndRights(page, 'Frontoffice', 1);
  });

  test('Add the role and rights for BE behandelaar @fill_empty_db', async ({ }) => {
    await addRoleAndRights(page, 'Backoffice', 2);
  });

  test('Go to the Overview page @fill_empty_db', async ({ }) => {
    await page.getByRole('button', { name: 'Volgende' }).click();
  });

  test('Set what is changed and save @fill_empty_db', async ({ }) => {
    await page.getByLabel('Algemeen').check();
    await page.getByLabel('Kenmerken').check();
    await page.getByLabel('Rechten').check();
    await page.getByPlaceholder('Geef omschrijving op').click();
    await page.getByPlaceholder('Geef omschrijving op').fill('Het object V2 is aangemaakt');
    await page.getByRole('button', { name: 'Opslaan' }).click();
    await page.waitForLoadState('domcontentloaded');
    await page.waitForLoadState('networkidle');
  });

  Object.values(testrondeDataArray).filter(
    attribute => attribute.magicString === 'auto_relatie_kenmerk_testronde_v2_object'
  ).forEach(attribute => {
    //The relation attribute for TESTRONDE v2 objects is being made here
    test(`Check and if not present make attribute ${attribute.nameAttribute} @fill_empty_db`, async ({ }) => {
      await checkOrAddAttribute({
        page,
        ...attribute,
        desiredFolderName: `${categoryAndFolder}`,
      });
    });
  });

});
