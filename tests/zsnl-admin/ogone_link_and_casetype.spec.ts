import { BrowserContext, expect, Page, test } from '@playwright/test';
import { baseUrl, beheerderUser, ogoneSha } from '../../e2e_tests';
import {
  checkOrAddCategoryOrFolder,
  logOff, openSystemAndLogin
} from '../../partial-tests';
import { checkOrAddAttribute } from '../../partial-tests/checkOrAddAttribute';
import { zsnlTestConfig } from '../../playwright.config';
import { allTestsBeforeAll, navigateAdministration, waitForCurrentCall } from '../../utils';

const { clickDelay, shortTimeout } = zsnlTestConfig;
let newAttributeMagicString = '';
const newAttribute = 'ZTC Reden van betaling';
const categoryOrFolderArray = [
  'ZTC zaaktypen',
  'ZTC kenmerken',
];
const attributesArray = [
  {
    nameAttribute: 'ZTC Bedrag', magicString: 'ztc_bedrag', publicName: 'Bedrag',
    title: 'Donatie bedrag', explanationExternal: 'Vul hier uw donatie bedrag in', sensitiveData: false,
    inputType: 'Valuta', explanationInternal: 'Een bedrag',
  },
];
const casetype = 'Betaling internetkassa Ingenico automatische test';
test.describe.serial('Set Ogone link, make simple casetype to test payment', () => {
  let page: Page;
  let context: BrowserContext;

  test.beforeAll(async ({ browser }, testInfo) => {
    ({ context, page } = await allTestsBeforeAll(context, browser, page, testInfo));

    await openSystemAndLogin({
      page,
      systemUrl: `${baseUrl}`,
      user: beheerderUser
    });
  });

  test.afterAll(async () => {
    await logOff({ page, systemUrl: `${baseUrl}` });
    await context.close();
  });

  test('Open admin section @fill_empty_db', async ({ }) => {
    await navigateAdministration({ page, menuItem: 'Catalogus' });
  });

  Object.values(categoryOrFolderArray).forEach(categoryOrFolder => {
    test(`Check and if not present make category ${categoryOrFolder} @fill_empty_db`, async ({ }) => {
      await checkOrAddCategoryOrFolder({ page, categoryOrFolder: `${categoryOrFolder}` });
    });
  });

  Object.values(attributesArray).forEach(attribute => {
    test(`Check and if not present make attribute ${attribute.nameAttribute} @fill_empty_db`, async ({ }) => {
      await checkOrAddAttribute({
        page,
        nameAttribute: `${attribute.nameAttribute}`,
        publicName: `${attribute.publicName}`,
        sensitiveData: (`${attribute.sensitiveData}` === 'true'),
        inputType: `${attribute.inputType}`,
        explanationInternal: `${attribute.explanationInternal}`,
        valueDefault: `${attribute.nameAttribute}`,
        desiredFolderName: `${categoryOrFolderArray[1]}`
      });
    });
  });

  test('Go to Koppelingconfiguratie in section Beheren @fill_empty_db', async ({ }) => {
    await page.locator('//*[@data-testid="MenuIcon"]').click();
    await page.getByRole('link', { name: 'Koppelingconfiguratie' }).click();
  });

  test('Start adding the Ogone link @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Voeg koppeling toe' }).nth(0)
      .click({ delay: clickDelay });
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('combobox').selectOption('string:ogone');
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Voeg koppeling toe' }).nth(1).click();
  });

  test('Fill the Ogone id @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Ogone ID\n\t\t\t *').click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Ogone ID\n\t\t\t *').fill('mintlab');
  });

  test('Fill the SHA-IN @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('SHA-IN versleuteling\n\t\t\t *').click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('SHA-IN versleuteling\n\t\t\t *').fill(`${ogoneSha}`);
  });

  test('Fill the SHA-OUT @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('SHA-1-OUT Versleuteling\n\t\t\t *').click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]')
      .getByLabel('SHA-1-OUT Versleuteling\n\t\t\t *').fill(`${ogoneSha}`);
  });

  test('Fill the Terugkeer-URL @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByPlaceholder('http://').click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByPlaceholder('http://').fill(`${baseUrl}`);
  });

  test('Fill the Order omschrijving @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Order omschrijving\n\t\t\t *').click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Order omschrijving\n\t\t\t *')
      .fill('[[zaaktype]] en [[omschrijving]]');
  });

  test('Set the link active @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Actief').first().check();
  });

  test('Save the Ogone link @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Opslaan' }).click();
  });

  test('Start adding a new casetype to test the Ogone link @fill_empty_db', async ({ }) => {
    await page.getByRole('link', { name: 'Catalogus' }).click();
    await page.waitForLoadState('domcontentloaded');
    await page.waitForLoadState('networkidle');
    await page.getByTestId('AddIcon').nth(1).click();
    await page.getByRole('button', { name: 'Zaaktype' }).click();
  });

  test('Choose the folder to store the new casetype @fill_empty_db', async ({ }) => {
    const locie = page.frameLocator('internal:attr=[title="Beheer formulier"i]')
      .locator('//select/option[contains(.//text(),"- ZTC zaaktypen")]').first();
    // eslint-disable-next-line playwright/no-conditional-in-test
    const optionNumber = await locie.getAttribute('value') || -1;
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('select[name="zaaktype\\.bibliotheek_categorie_id"]')
      .selectOption(optionNumber.toString());
  });

  test('Name the new casetype @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="node\\.titel"]')
      .fill(casetype);
  });

  test('Fill the Identificatie @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="node\\.code"]').fill('ZSNL-AUTTEST-002');
  });

  test('Fill the Aanleiding @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="node\\.properties\\.aanleiding"]')
      .fill('automatische testen');
  });

  test('Fill the Doel @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="node\\.properties\\.doel"]')
      .fill('Testen betaling');
  });

  test('Fill the Wettelijke grondslag @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('textarea[name="definitie\\.grondslag"]').fill('Geen');
  });

  test('Set the Doorlooptijd wettelijk @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="definitie\\.afhandeltermijn"]').fill('1');
  });

  test('Set the Doorlooptijd service @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="definitie\\.servicenorm"]').fill('1');
  });

  test('Go to section relaties @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Volgende' }).click();
    await waitForCurrentCall({ page });
  });

  test('Set the possible Aanvragers @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Natuurlijk Persoon (Binnengemeentelijk)').check();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Natuurlijk Persoon (Buitengemeentelijk)').check();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Niet natuurlijk persoon (Organisatie)').check();
  });

  test('Set the amounts @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="params\\.definitie\\.pdc_tarief"]')
      .fill('1.00');
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]')
      .locator('input[name="params\\.node\\.properties\\.pdc_tarief_balie"]').fill('2.00');
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]')
      .locator('input[name="params\\.node\\.properties\\.pdc_tarief_telefoon"]').fill('3.00');
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]')
      .locator('input[name="params\\.node\\.properties\\.pdc_tarief_email"]').fill('4.00');
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]')
      .locator('input[name="params\\.node\\.properties\\.pdc_tarief_behandelaar"]').fill('5.00');
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]')
      .locator('input[name="params\\.node\\.properties\\.pdc_tarief_post"]').fill('6.00');
  });

  test('Go to section acties @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Volgende' }).click();
    await waitForCurrentCall({ page });
  });

  test('Check Aanvragen met publiek webformulier @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Aanvragen met publiek webformulier').check();
  });

  test('Check Internetkassa gebruiken @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Internetkassa gebruiken').check();
  });

  test('Check Anders betalen toestaan @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Anders betalen toestaan').check();
  });

  test('Check E-mailadres verplicht bij webformulier @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('E-mailadres verplicht bij webformulier').check();
  });

  test('Check Zaak automatisch in behandeling nemen @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Zaak automatisch in behandeling nemen').check();
  });

  test('Go to section Fase Mijlpalen benoemen @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Volgende' }).click();
    await waitForCurrentCall({ page });
  });

  test('Set department configureren @fill_empty_db', async ({ }) => {
    const locie = page.frameLocator('internal:attr=[title="Beheer formulier"i]')
      .locator('//label[contains(.//text(),"Afdeling")]/parent::div//select/option[contains(.//text(),"Frontoffice")]');
    // eslint-disable-next-line playwright/no-conditional-in-test
    const optionNumber = await locie.getAttribute('value') || -1;
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]')
      .locator('select[name="statussen\\.1\\.definitie\\.ou_id"]').selectOption(optionNumber.toString());
  });

  test('Go to section Fase 1 "Registreren" configureren @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Volgende' }).click();
    await waitForCurrentCall({ page });
  });

  test('Add group Uitleg test @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('link', { name: 'Voeg groep toe' }).click();
  });

  test('Name group Uitleg test @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('textbox').fill('Uitleg test');
  });

  test('Add Toelichting of group Uitleg test @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('paragraph').click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('#quill_kenmerken_help div').first()
      .fill('Dit formulier dient om de ogone/inigenico betalings koppeling te testen');
  });

  test('Check Toon tekstvak in PIP for group Uitleg test@fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('checkbox').check();
    await expect(page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('checkbox')).toBeChecked();
  });

  test('Save group Uitleg test @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Opslaan' })
      .click();
    await expect(page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('cell', { name: 'Uitleg test' })).toBeVisible();
    await waitForCurrentCall({ page });
  });

  test('Add Textblok Test betaling @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('link', { name: ' Tekstblok' }).click();
  });

  test('Name Textblok Test betaling @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('textbox').fill('Test betaling');
  });

  test('Add Inhoud Tekstblok Test betaling in bold @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('paragraph').click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('#quill_kenmerken_text_content div').first()
      .fill('Dit is een test voor de Ogone betaling');
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByText('Dit is een test voor de Ogone betaling').click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]')
      .locator('#quill_kenmerken_text_content div').filter({ hasText: 'Dit is een test voor de Ogone betaling' }).press('Control+a');
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('button[class="ql-bold"]').nth(1).click();
  });

  test('Check Toon tekstvak in PIP for Tekstblok Test Betaling @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('checkbox').check();
    await expect(page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('checkbox')).toBeChecked();
  });

  test('Save Textblok Test betaling @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Opslaan' })
      .click();
    await waitForCurrentCall({ page });
  });

  test('Add group Betaling @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('link', { name: 'Voeg groep toe' }).click();
  });

  test('Name group Betaling @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('textbox').fill('Betaling');
  });

  test('Add Toelichting of group Betaling @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('button[class="ql-underline"]').nth(1).click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('#quill_kenmerken_help div').first()
      .fill('Dit zijn de betalingsgegevens');
  });

  test('Check Toon tekstvak in PIP for group Betaling @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('checkbox').check();
    await expect(page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('checkbox')).toBeChecked();
  });

  test('Save group Betaling @fill_empty_db', async ({ }) => {
    page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Opslaan' })
      .click();
    await waitForCurrentCall({ page });
  });

  test(`Add Nieuw kenmerk ${newAttribute} @fill_empty_db`, async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('link', { name: ' Kenmerk' }).click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Nieuw Kenmerk' }).click();
  });

  test(`Add Kenmerk naam ${newAttribute} @fill_empty_db`, async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="kenmerk_naam"]')
      .fill(`${newAttribute}`);
  });

  test(`Add Publieke naam Kenmerk ${newAttribute} @fill_empty_db`, async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="kenmerk_naam_public"]')
      .fill('Uw reden betaling');
  });

  test(`Select Invoertype for ${newAttribute} @fill_empty_db`, async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('#kenmerk_invoertype').selectOption('richtext');
  });

  test(`Select the Categorie for ${newAttribute} @fill_empty_db`, async ({ }) => {
    const locie = page.frameLocator('internal:attr=[title="Beheer formulier"i]')
      .locator('//select[@name="bibliotheek_categorie_id"]/option[contains(.//text(),"- ZTC kenmerken")]').first();
    // eslint-disable-next-line playwright/no-conditional-in-test
    const optionNumber = await locie.getAttribute('value') || -1;
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('select[name="bibliotheek_categorie_id"]')
      .selectOption(optionNumber.toString());
  });

  test(`Fill the explanationInternal for ${newAttribute} @fill_empty_db`, async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('textarea[name="kenmerk_help"]')
      .click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('textarea[name="kenmerk_help"]')
      .fill('Geef hier de reden van uw betaling');
  });

  test(`Save the magic string and the new attribute ${newAttribute} @fill_empty_db`, async ({ }) => {
    newAttributeMagicString = await page.frameLocator('iframe[title="Beheer formulier"]')
      .locator('input[name="kenmerk_magic_string"]').inputValue();

    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Opslaan' })
      .click({ delay: clickDelay });
    await waitForCurrentCall({ page });
    await expect(page.getByText('Referentieel')).toBeHidden();
  });

  test(`Open for edit the new ${newAttribute} @fill_empty_db`, async ({ }) => {
    await page.frameLocator('iframe[title="Beheer formulier"]').getByRole('cell', {
      name:
        ` Naam: ZTC Reden van betaling Titel: - Type: Rich text Magic string: [[${newAttributeMagicString}]] ` +
        'Gepubliceerde versie: niet beschikbaar Actuele versie: 1  '
    })
      .getByRole('link', { name: '' }).click();
    // await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('(//a[@title="Kenmerk bewerken"])[1]').click();
  });

  test(`Set the title for ${newAttribute} @fill_empty_db`, async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('textbox').click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('textbox').fill('Reden van donatie');
  });

  test(`Set the external help for ${newAttribute} @fill_empty_db`, async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('#quill_kenmerken_help_extern')
      .getByRole('paragraph').click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('#quill_kenmerken_help_extern div')
      .first().fill('Geef hier de reden van uw donatie');
  });

  test(`Set the visibility on PIP for ${newAttribute} @fill_empty_db`, async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="kenmerken_pip"]').check();
  });

  test(`Save again the new attribute ${newAttribute} @fill_empty_db`, async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Opslaan' })
      .click({ delay: clickDelay });
  });

  test(`Start adding Kenmerk ${attributesArray[0].nameAttribute} through searching @fill_empty_db`, async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('link', { name: ' Kenmerk' }).click();
  });

  test(`Fill searchtext ${attributesArray[0].nameAttribute} @fill_empty_db`, async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('textbox').fill(`${attributesArray[0].nameAttribute}`);
  });

  test(`Search for Kenmerk ${attributesArray[0].nameAttribute} @fill_empty_db`, async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Zoeken' }).click();
  });

  test(`Select result ${attributesArray[0].nameAttribute} @fill_empty_db`, async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('cell',
      { name: `${attributesArray[0].nameAttribute}` }).first()
      .click();
    await waitForCurrentCall({ page });
  });

  test(`Open for edit the new ${attributesArray[0].nameAttribute} @fill_empty_db`, async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]')
      .getByRole('cell', {
        name: ` Naam: ${attributesArray[0].nameAttribute} Titel: - Type: Valuta Magic string: ` +
          `[[${attributesArray[0].magicString}` +
          ']] Gepubliceerde versie: niet beschikbaar Actuele versie: 1  '
      })
      .getByRole('link', { name: '' }).click();
  });

  test(`Set the title for ${attributesArray[0].nameAttribute} @fill_empty_db`, async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('textbox').click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('textbox').fill(`${attributesArray[0].title}`);
  });

  test(`Set the external help for ${attributesArray[0].nameAttribute} @fill_empty_db`, async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('#quill_kenmerken_help_extern')
      .getByRole('paragraph').click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('#quill_kenmerken_help_extern div')
      .first().fill(`${attributesArray[0].explanationExternal}`);
  });

  test(`Set the visibility on PIP for ${attributesArray[0].nameAttribute} @fill_empty_db`, async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="kenmerken_pip"]').check();
  });

  test(`Save the changed attribute ${attributesArray[0].nameAttribute} @fill_empty_db`, async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Opslaan' })
      .click({ delay: clickDelay });
  });

  test('Start adding Regels @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]')
      .getByRole('tab', { name: 'Regels (0)' }).locator('span').first().click();
    const rulesExpanded = await page.frameLocator('internal:attr=[title="Beheer formulier"i]')
      .locator('//h3//a[contains(.//text(), "Regels")]/parent::h3').getAttribute('aria-expanded');
    expect(rulesExpanded).toBe('true');
  });

  test('Add group NP @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('link', { name: 'Voeg groep toe' }).click();
  });

  test('Add Name group NP @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('textbox').first().click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('textbox').first().fill('NP');
  });

  test('Save group NP @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Opslaan' })
      .click();
    await waitForCurrentCall({ page });
  });

  test('Go to section Fase 2 Fase "Afhandelen" configureren @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Volgende' }).click();
    await waitForCurrentCall({ page });
  });

  test('Start adding to Resultaten @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('link', { name: 'Resultaten (0)' }).click();
  });

  test('Add Afgehandeld label @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('link', { name: ' Resultaat' }).click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="resultaten_label"]').fill('Afgehandeld');
  });

  test('Select afgehandeld result @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]')
      .locator('select[name="resultaten_resultaat"]').selectOption('afgehandeld');
  });

  test('Add Selectielijst @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]')
      .locator('input[name="resultaten_selectielijst"]').fill('Selectielijst voor gemeenten en intergemeentelijke organen 2020');
  });

  test('Add Brondatum @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('#resultaten_selectielijst_brondatum_input')
      .fill('01-01-2020');
  });

  test('Select Bewaartermijn @fill_empty_db', async ({ }) => {
    const locie = page.frameLocator('internal:attr=[title="Beheer formulier"i]')
      .locator('//select[@name="resultaten_bewaartermijn"]/option[contains(.//text(),"4 weken")]').first();
    // eslint-disable-next-line playwright/no-conditional-in-test
    const optionNumber = await locie.getAttribute('value') || -1;
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('select[name="resultaten_bewaartermijn"]')
      .click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('select[name="resultaten_bewaartermijn"]')
      .selectOption(optionNumber.toString());
  });

  test('Select Specifiek benoemde wet- en regelgeving @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('select[name="resultaten_properties_herkomst"]')
      .click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('select[name="resultaten_properties_herkomst"]')
      .selectOption('Specifiek benoemde wet- en regelgeving');
  });

  test('Save the result @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Opslaan' })
      .click();
  });

  test('Go to section rechten @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Volgende' }).click();
    await waitForCurrentCall({ page });
  });

  test('Start adding a role @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: ' Rol toevoegen' }).first()
      .click({ delay: clickDelay });
  });

  test('Select Afdeling @fill_empty_db', async ({ }) => {
    const locie = page.frameLocator('internal:attr=[title="Beheer formulier"i]')
      .locator('//span[contains(.//text(),"Afdeling")]/parent::div//select/option[contains(.//text(),"Frontoffice")]');
    // eslint-disable-next-line playwright/no-conditional-in-test
    const optionNumber = await locie.getAttribute('value') || -1;
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('cell', { name: 'Afdeling Rol' }).locator('div')
      .getByRole('combobox').nth(0).selectOption(optionNumber.toString());
  });

  test('Select rol @fill_empty_db', async ({ }) => {
    const locie = page.frameLocator('internal:attr=[title="Beheer formulier"i]')
      .locator('//span[contains(.//text(),"Rol")]/parent::div//select/option[contains(.//text(),"Behandelaar")]');
    // eslint-disable-next-line playwright/no-conditional-in-test
    const optionNumber = await locie.getAttribute('value') || -1;
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('cell', { name: 'Afdeling Rol' }).locator('div')
      .filter({ hasText: 'Rol AdministratorAfdelingshoofdApp gebruikerBasisregistratiebeheerderBehandelaar' })
      .getByRole('combobox').selectOption(optionNumber.toString());
  });

  test('Select actions @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Mag zaken behandelen').check();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Mag zaken raadplegen').check();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Mag zaken zoeken').check();
  });

  test('First time go to section afronden @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Volgende' }).click();
    await waitForCurrentCall({ page });
  });

  test('Select changed sections @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Basisattributen').check();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Zaakacties').check();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Fasering en toewijzing').check();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Kenmerken').check();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Regels').check();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Autorisatie').check();
  });

  test('Give reason for changes @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('textbox').click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('textbox').fill('Nieuw zaaktype');
  });

  test('First time publish casetype @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Publiceren' }).click();
  });

  test('Search the new casetype @fill_empty_db', async ({ }) => {
    await page.getByPlaceholder('Zoeken in de catalogus').click();
    await page.getByPlaceholder('Zoeken in de catalogus').fill(casetype);
    await page.getByPlaceholder('Zoeken in de catalogus').press('Enter');
  });

  test('Open the new casetype @fill_empty_db', async ({ }) => {
    await page.getByLabel('', { exact: true }).first().check();
    await page.locator('button[name="buttonBarEditCaseType"]').click();
  });

  test('Second time go to Fase 1 "Registreren" configureren @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('link', { name: '1' }).click();
  });

  test('Start adding a Regel @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('link', { name: ' Regel' }).click();
  });

  test('Name the new Regel @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByPlaceholder('Vul hier de naam van de regel in').fill('NP');
  });

  test('Select voorwaarden @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('#regels_voorwaarde_1_valuecheckbox_1').check();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Natuurlijk persoon', { exact: true }).check();
  });

  test(`Select action to do for ${attributesArray[0].nameAttribute} @fill_empty_db`, async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]')
      .locator('select[name="regels_actie_1"]').selectOption('vul_waarde_in_met_formule');
    await waitForCurrentCall({ page });
  });

  test(`Select action attribute for ${attributesArray[0].nameAttribute} @fill_empty_db`, async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]')
      .locator('select[name="regels_actie_1_kenmerk"]').selectOption('price');
  });

  test(`Fill in the formula ${attributesArray[0].nameAttribute} @fill_empty_db`, async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').locator('input[name="regels_actie_1_formula"]').click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]')
      .locator('input[name="regels_actie_1_formula"]').fill(`${attributesArray[0].magicString}`);
    try {
      await expect(page.getByText(`${attributesArray[0].magicString}`)).toBeVisible({ timeout: shortTimeout });
    }
    catch {
      await page.frameLocator('internal:attr=[title="Beheer formulier"i]')
        .locator('input[name="regels_actie_1_formula"]').fill(`${attributesArray[0].magicString}`);
    }
  });

  test('Save Regel @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Opslaan' })
      .click();
    await waitForCurrentCall({ page });
  });

  test('Second time go to section afronden @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Volgende' }).click();
    await waitForCurrentCall({ page });
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Volgende' }).click();
    await waitForCurrentCall({ page });
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Volgende' }).click();
    await waitForCurrentCall({ page });
  });

  test('Second time select changed section @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Regels').check();
  });

  test('Second time fill reason change @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('textbox').click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('textbox').fill('Regel toegevoegd');
  });

  test('Second time publish @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Publiceren' }).click();
  });

});