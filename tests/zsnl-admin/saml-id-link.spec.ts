import { BrowserContext, Page, test } from '@playwright/test';
import { baseUrl, beheerderUser } from '../../e2e_tests';
import {
  logOff, openSystemAndLogin
} from '../../partial-tests';
import { zsnlTestConfig } from '../../playwright.config';
import { allTestsBeforeAll, navigateAdministration } from '../../utils';

const { clickDelay } = zsnlTestConfig;
const systemUrl = `${baseUrl}`;

test.describe.serial('Sets a new spoof SAML Identity provider link', () => {
  let page: Page;
  let context: BrowserContext;

  test.beforeAll(async ({ browser }, testInfo) => {
    ({ context, page } = await allTestsBeforeAll(context, browser, page, testInfo));

    await openSystemAndLogin({
      page,
      systemUrl,
      user: beheerderUser
    });
  });

  test.afterAll(async () => {
    await logOff({ page, systemUrl });
    await context.close();
  });

  test('Go to Koppelingconfiguratie in section Beheren @fill_empty_db', async ({ }) => {
    await navigateAdministration({ page, menuItem: 'Koppelingconfiguratie' });
  });

  test('Start adding the SAML 2.0 Identity Provider link @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Voeg koppeling toe' }).nth(0)
      .click({ delay: clickDelay });
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('combobox').selectOption('string:samlidp');
  });

  test('Name the SAML link @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Naam\n\t\t\t *').click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Naam\n\t\t\t *')
      .fill('SAML IdP: DigiD development');
  });

  test('Add the SAML link @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Voeg koppeling toe' }).nth(1).click();
  });

  test('Select the SAML Implementatie @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]')
      .locator('select[name="interface_saml_type"]').selectOption('string:spoof');
  });

  test('Select the SAML IdP metadata-URL @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('SAML IdP metadata-URL\n\t\t\t *').click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('SAML IdP metadata-URL\n\t\t\t *')
      .fill('https://was.digid.nl/saml/idp/metadata');
  });

  test('Select to use on civilian form/login pages @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('checkbox').first().check();
  });

  test('Set the link active @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Actief').first().check();
  });

  test('Save the SAML ID link @fill_empty_db', async ({ }) => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Opslaan' }).click();
  });

});
