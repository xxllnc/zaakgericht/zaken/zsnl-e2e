import { BrowserContext, expect, Page, test } from '@playwright/test';
import { baseUrl, beheerderUser } from '../../e2e_tests';
import {
  logOff, openSystemAndLogin, checkOrAddCategoryOrFolder, whatsChangedAndPublish
} from '../../partial-tests';
import { zsnlTestConfig } from '../../playwright.config';
import { navigateAdministration } from '../../utils';
import { personDataDev } from '../../testfiles/personData';
import { testrondeDataArray } from '../../testfiles/TESTRONDE/testrondeTestdata';

const { options, typeDelay, longTimeout, shortTimeout } = zsnlTestConfig;
const systemUrl = `${baseUrl}`;
const BSN = personDataDev[2].BSN;
const personSearchName = personDataDev[2].familyName;
const casetypes = [
  'Auto TESTRONDE Deelzaak V2',
  'Auto TESTRONDE Gerelateerde zaak V2',
  'Auto TESTRONDE Vervolgzaak V2',
  'Auto TESTRONDE V2',
];
const categoryOrFolder = 'Automatische E2E TESTRONDE';

async function setImportSettings(page: Page, toggleText: string, attributeType: string) {
  await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByText(`${toggleText}`).first().click();
  const locie = page.frameLocator('internal:attr=[title="Beheer formulier"i]')
    .locator(`//select/option[contains(.//text(),"- ${categoryOrFolder}")]`).first();
  // eslint-disable-next-line playwright/no-conditional-in-test
  const optionNumber = await locie.getAttribute('value') || -1;
  await page.frameLocator('internal:attr=[title="Beheer formulier"i]')
    .locator('select[name="bibliotheek_categorie_id"]')
    .selectOption(optionNumber.toString());
  await page.frameLocator('iframe[title="Beheer formulier"]')
    .getByLabel(`Geselecteerde categorie voor alle nieuwe ${attributeType} gebruiken`).check();
  await page.frameLocator('iframe[title="Beheer formulier"]').locator('button[name="action"]').click();

  await expect(page.frameLocator('iframe[title="Beheer formulier"]').locator('#add')
    .getByText(`${attributeType} toe`))
    .toBeHidden({ timeout: longTimeout });
}

Object.values(casetypes).forEach(casetype => {
  const casetypeFile = `${casetype.replace(/\s/g, '_')}.ztb`;
  let page: Page;
  test.describe.serial(`TR PRE 2 Import ${casetype}`, async () => {
    let context: BrowserContext;

    test.beforeAll(async ({ browser }) => {
      context = await browser.newContext(options);
      page = await context.newPage();
      await openSystemAndLogin({
        page,
        systemUrl,
        user: beheerderUser
      });
    });

    test.afterAll(async () => {
      await logOff({ page, systemUrl });
      await context.close();
    });

    test(`Open admin section  ${casetype} @fill_empty_db`, async ({ }) => {
      await navigateAdministration({ page, menuItem: 'Catalogus' });
    });

    test(`Case: ${casetype}: Check and if not present make category ${categoryOrFolder} @fill_empty_db`, async ({ }) => {
      await checkOrAddCategoryOrFolder({ page, categoryOrFolder: `${categoryOrFolder}` });
    });

    test(`Start import ${casetype} @fill_empty_db`, async ({ }) => {
      await page.getByTestId('AddIcon').nth(1).click();
      await page.getByRole('button', { name: 'Importeren' })
        .click();
      await page.waitForLoadState('domcontentloaded');
    });

    test(`Select Zaaktypen ${casetype} @fill_empty_db`, async ({ }) => {
      await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Zaaktypen')
        .check();
    });

    test(`Select Zaaksysteem.nl ${casetype} @fill_empty_db`, async ({ }) => {
      await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByLabel('Zaaksysteem.nl')
        .check();
    });

    test(`Add case type file ${casetype} @fill_empty_db`, async () => {
      const fileChooserPromise = page.waitForEvent('filechooser');
      await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Kies bestand' })
        .click();
      const fileChooser = await fileChooserPromise;
      await fileChooser.setFiles(`testfiles/TESTRONDE/${casetypeFile}`);
    });

    test(`Wait for upload then click volgende ${casetype} @fill_empty_db`, async () => {
      await expect(page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByText(`${casetypeFile}`)).toBeVisible();
      await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Volgende' })
        .click();
    });

    test(`Expect heading Zaaktypen to be visible  ${casetype} @fill_empty_db`, async ({ }) => {
      await expect(page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('heading',
        { name: 'Zaaktypen' }))
        .toBeVisible();
    });

    if (casetype.endsWith('TESTRONDE V2')) {
      test(`Case: ${casetype}: Select folder for casetype @fill_empty_db`, async ({ }) => {
        await setImportSettings(page, `${casetype} (MAIN)`, 'zaaktype');
      });

      test(`Case: ${casetype}: Select folder for templates @fill_empty_db`, async ({ }) => {
        await setImportSettings(page, 'Auto alle kenmerken als DOCX', 'sjabloon');
      });

      test(`Case: ${casetype}: Select folder for notifications @fill_empty_db`, async ({ }) => {
        await setImportSettings(page, 'Auto Testronde registratie betrokkene', 'bericht');
      });

      test(`Case: ${casetype}: Select betrokkene @fill_empty_db`, async ({ }) => {
        await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByText('Natuurlijk persoon').click();
        await page.frameLocator('iframe[title="Beheer formulier"]').getByPlaceholder('Typ hier uw zoekterm').click();
        await page.frameLocator('iframe[title="Beheer formulier"]').getByPlaceholder('Typ hier uw zoekterm').fill(BSN);
        await expect(page.frameLocator('iframe[title="Beheer formulier"]').getByText(personSearchName)).toBeVisible();
        await page.frameLocator('iframe[title="Beheer formulier"]').getByPlaceholder('Typ hier uw zoekterm').press('ArrowDown');
        await page.frameLocator('iframe[title="Beheer formulier"]').getByPlaceholder('Typ hier uw zoekterm').press('Enter');
        await page.frameLocator('iframe[title="Beheer formulier"]').locator('button[name="action"]').click();

        await expect(page.frameLocator('iframe[title="Beheer formulier"]').locator('#add')
          .getByText('Voeg meegeleverde betrokkene rol toe'))
          .toBeHidden({ timeout: longTimeout });
      });
    }

    test(`Case: ${casetype}: Import casetype  @fill_empty_db`, async ({ }) => {
      await page.frameLocator('internal:attr=[title="Beheer formulier"i]')
        .getByRole('button', { name: 'Importeren' })
        .click();
      await page.waitForLoadState('domcontentloaded');
    });

    test(`Find adding of casetype ${casetype} @fill_empty_db`, async ({ }) => {
      await page.getByRole('link', { name: 'Catalogus Catalogus' }).click();
      await page.getByPlaceholder('Zoeken in de catalogus').click();
      await page.getByPlaceholder('Zoeken in de catalogus').fill(casetype);
      await page.getByPlaceholder('Zoeken in de catalogus').press('Enter', { delay: typeDelay });
      await page.waitForLoadState('domcontentloaded');
      await page.waitForLoadState('networkidle');
      const indexMax = 5;
      for (let index = 0; index < indexMax; index++) {
        try {
          await expect(page.getByRole('link', { name: `${casetype}` })).toBeVisible({ timeout: shortTimeout });
          index = indexMax;
        }
        catch {
          await page.reload({ waitUntil: 'domcontentloaded' });
        }
      }
    });

    if (!casetype.endsWith('TESTRONDE V2')) {
      test(`Open imported casetype ${casetype} @fill_empty_db`, async ({ }) => {
        await page.getByText(casetype, { exact: true }).click();
      });

      test(`Goto phase 2 to add v2 relation object attribute for ${casetype} @fill_empty_db`, async ({ }) => {
        await page.frameLocator('iframe[title="Beheer formulier"]').getByRole('link', { name: '2', exact: true }).click();
      });

      test(`Add v2 relation object attribute for ${casetype} @fill_empty_db`, async ({ }) => {
        await page.frameLocator('iframe[title="Beheer formulier"]').getByRole('link', { name: ' Kenmerk' }).scrollIntoViewIfNeeded();
        await page.frameLocator('iframe[title="Beheer formulier"]').getByRole('link', { name: ' Kenmerk' }).click();
        await page.frameLocator('iframe[title="Beheer formulier"]').getByRole('textbox').click();
        await page.frameLocator('iframe[title="Beheer formulier"]').getByRole('textbox').fill('auto_relatie_kenmerk_testronde_v2_object');
        await page.frameLocator('iframe[title="Beheer formulier"]').getByRole('button', { name: 'Zoeken' }).click();
        await expect(page.frameLocator('iframe[title="Beheer formulier"]').getByRole('textbox')).toBeHidden();
        await page.frameLocator('iframe[title="Beheer formulier"]').getByRole('tabpanel', { name: 'Resultaten' })
          .getByRole('cell', { name: 'auto_relatie_kenmerk_testronde_v2_object' }).click();
      });

      test(`Remove imported v2 relation object attribute for ${casetype} @fill_empty_db`, async ({ }) => {
        await page.frameLocator('iframe[title="Beheer formulier"]').getByRole('cell',
          { name: ' Naam: auto_relatie_kenmerk_testronde_v2_object Titel: TESTRONDE V2 object Type: Relatie Magic string: [[auto_relatie_kenmerk_testronde_v2_object]] Gepubliceerde versie: 1 Actuele versie: 1  ' })
          .getByRole('link', { name: '' }).first().click();
        await page.waitForLoadState('domcontentloaded');
      });

      test(`Open to edit v2 relation object attribute for ${casetype} @fill_empty_db`, async ({ }) => {
        await page.frameLocator('iframe[title="Beheer formulier"]')
          .getByRole('cell', {
            name: ' Naam: auto_relatie_kenmerk_testronde_v2_object Titel: ' +
              '- Type: Relatie Magic string: [[auto_relatie_kenmerk_testronde_v2_object]] ' +
              'Gepubliceerde versie: niet beschikbaar Actuele versie: 1  '
          }).getByRole('link', { name: '' }).click();
      });

      test(`Set mandatory for v2 relation object attribute for ${casetype} @fill_empty_db`, async ({ }) => {
        await page.frameLocator('iframe[title="Beheer formulier"]').locator('input[name="kenmerken_value_mandatory"]').check();
      });

      test(`Set title for v2 relation object attribute for ${casetype} @fill_empty_db`, async ({ }) => {
        await page.frameLocator('iframe[title="Beheer formulier"]').locator('input[name="kenmerken_label"]').click();
        await page.frameLocator('iframe[title="Beheer formulier"]').locator('input[name="kenmerken_label"]')
          .fill(testrondeDataArray.auto_relatie_kenmerk_testronde_v2_object.title !== undefined ?
            testrondeDataArray.auto_relatie_kenmerk_testronde_v2_object.title : '');
      });

      test(`Check show and edit on PIP for v2 relation object attribute for ${casetype} @fill_empty_db`, async ({ }) => {
        await page.frameLocator('iframe[title="Beheer formulier"]').locator('input[name="kenmerken_pip"]').check();
        await page.frameLocator('iframe[title="Beheer formulier"]').locator('input[name="kenmerken_pip_can_change"]').check();
      });

      test(`Set external help for v2 relation object attribute for ${casetype} @fill_empty_db`, async ({ }) => {
        await page.frameLocator('iframe[title="Beheer formulier"]').locator('#quill_kenmerken_help_extern').getByRole('paragraph').click();
        await page.frameLocator('iframe[title="Beheer formulier"]').locator('#quill_kenmerken_help_extern div').first()
          .fill(testrondeDataArray.auto_relatie_kenmerk_testronde_v2_object.explanationExternal !== undefined ?
            testrondeDataArray.auto_relatie_kenmerk_testronde_v2_object.explanationExternal : '');
      });

      test(`Check button make object for v2 relation object attribute for ${casetype} @fill_empty_db`, async ({ }) => {
        await page.frameLocator('iframe[title="Beheer formulier"]').locator('input[name="kenmerken_custom_object_create"]').check();
      });

      test(`Set setting button make object title for v2 relation object attribute for ${casetype} @fill_empty_db`, async ({ }) => {
        await page.frameLocator('iframe[title="Beheer formulier"]').getByPlaceholder('Aanmaken Auto TESTRONDE Objecttype V2').click();
        await page.frameLocator('iframe[title="Beheer formulier"]').getByPlaceholder('Aanmaken Auto TESTRONDE Objecttype V2')
          .fill('Object V2 aanmaken via relatiekenmerk');
      });

      Object.values(testrondeDataArray).filter(
        attribute => attribute.inObject
      ).forEach(attribute => {
        test(`${casetype}: Add attribute ${attribute.nameAttribute} prefill in object @fill_empty_db`, async ({ }) => {
          await page.frameLocator('iframe[title="Beheer formulier"]')
            .locator(`input[name="kenmerken_custom_object_attr_${attribute.magicString}"]`).click();
          // eslint-disable-next-line playwright/no-conditional-in-test
          attribute.magicString === 'auto_tst_o0001_object_naam' ?
            await page.frameLocator('iframe[title="Beheer formulier"]')
              .locator(`input[name="kenmerken_custom_object_attr_${attribute.magicString}"]`)
              .fill('Geef hier een naam die de objecttitel uniek maakt') :
            await page.frameLocator('iframe[title="Beheer formulier"]')
              .locator(`input[name="kenmerken_custom_object_attr_${attribute.magicString}"]`)
              .fill(`[[${attribute.magicString}]]`);
        });
      });

      test(`${casetype}: Save the V2 relation object settings  @fill_empty_db`, async ({ }) => {
        await page.frameLocator('iframe[title="Beheer formulier"]').getByRole('button', { name: 'Opslaan' }).click();
      });

      test(`${casetype}: finish and save @fill_empty_db`, async ({ }) => {
        await whatsChangedAndPublish({
          page, whatsChangedArray: ['Kenmerken'], publishText:
            `V2 object added in ${casetype}`, testScriptName: casetype
        });
      });
    }
  });
});
