import { BrowserContext, Page, test } from '@playwright/test';
import { baseUrl, beheerderUser } from '../../e2e_tests';
import {
  checkOrAddPersonContact,
  logOff, openSystemAndLogin
} from '../../partial-tests';
import { allTestsBeforeAll } from '../../utils';

const systemUrl = `${baseUrl}`;
test.describe.serial('Check if BSN is present, if not add, if present check and reset', () => {
  let page: Page;
  let context: BrowserContext;
  const BSNArray = [
    '010082426',
    '547608238',
    '132915947',
  ];

  test.beforeAll(async ({ browser }, testInfo) => {
    ({ context, page } = await allTestsBeforeAll(context, browser, page, testInfo));

    await openSystemAndLogin({
      page,
      systemUrl,
      user: beheerderUser
    });
  });

  test.afterAll(async () => {
    await logOff({ page, systemUrl });
    await context.close();
  });

  Object.values(BSNArray).forEach(BSN => {
    test(`Check and if not present make contact ${BSN}`, async ({ }, testInfo) => {
      await checkOrAddPersonContact({ page, testTitle: testInfo.title, BSN: `${BSN}` });
    });
  });
});

