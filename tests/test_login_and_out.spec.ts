import { BrowserContext, expect, Page, test } from '@playwright/test';
import { baseUrl, beheerderUser } from '../e2e_tests';
import {
  logOff, openSystemAndLogin
} from '../partial-tests';
import { zsnlTestConfig } from '../playwright.config';
import { allTestsBeforeAll, locators } from '../utils';

const { longTimeout } = zsnlTestConfig;
const systemUrl = `${baseUrl}`;

test.describe.serial('Testing logging in and out', () => {
  let page: Page;
  let context: BrowserContext;

  test.beforeAll(async ({ browser }, testInfo) => {
    ({ context, page } = await allTestsBeforeAll(context, browser, page, testInfo));

    await openSystemAndLogin({
      page,
      systemUrl,
      user: beheerderUser
    });
  });

  test.afterAll(async () => {
    await logOff({ page, systemUrl });
    await context.close();
  });

  test('Check being logged in', async ({ }) => {
    await expect(page.locator(locators.partialTextLocator('Mijn openstaande zaken'))).toBeVisible({ timeout: longTimeout });
  });

});

