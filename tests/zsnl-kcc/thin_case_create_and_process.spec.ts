import { BrowserContext, expect, Page, test } from '@playwright/test';
import { format } from 'date-fns';
import { baseUrl, behandelaarUser } from '../../e2e_tests';
import {
  createNewCase, logOff, openSystemAndLogin
} from '../../partial-tests';
import { allTestsBeforeAll, checkTextReload, getCaseNumber, locators } from '../../utils';
import { checkDateShown } from '../../utils/checkDateShown';
import { fillDate } from '../../utils/fillDate';
import { zsnlTestConfig } from '../../playwright.config';

const { extreemLongTimeout, superLongTimeout, longTimeout, clickDelaySlow } = zsnlTestConfig;
const systemUrl = `${baseUrl}`;
const dateTimeStamp = format(new Date(), 'yyyy-MM-dd HH:mm:ss.SSS');
const caseType = 'Basiszaaktype dun automatische test';
let caseNumberInMessage = '00000000';
let caseNumberInUrl = '00000000';

for (const caseNo of Array(3).keys()) {
  const caseTypeAndNo = `${caseType} nr ${caseNo + 1}`;

  test.describe.serial(`Make a ${caseTypeAndNo} case and treat the case`, () => {
    let page: Page;
    let context: BrowserContext;

    test.beforeAll(async ({ browser }, testInfo) => {
      ({ context, page } = await allTestsBeforeAll(context, browser, page, testInfo));

      await openSystemAndLogin({
        page,
        systemUrl,
        user: behandelaarUser
      });
    });

    test.afterAll(async () => {
      await logOff({ page, systemUrl });
      await context.close();
    });

    test(`Start new case ${caseType}`, async ({ }) => {
      await createNewCase({ page, caseType });
    });

    test('Fill Onderwerp', async ({ }) => {
      await page.getByLabel('Onderwerp*').fill(`Het onderwerp op ${dateTimeStamp}`);
    });

    test('Fill Taakveld', async ({ }) => {
      await page.getByRole('combobox', { name: 'Taakveld' }).selectOption('string:Inwoners');
    });

    test('Fill Samenvatting', async ({ }) => {
      await page.locator('textarea').fill(`De samenvatting van deze zaak van zaaktype ${caseType}`);
    });

    test('Fill Locatie', async ({ }) => {
      await page.getByLabel('Locatie (indien van toepassing)*').fill('dordrecht bud');
      await page.getByRole('button', { name: 'Buddingh\'plein 22, 3311BV Dordrecht' }).click();
    });

    test('Fill Ontvangstdatum', async ({ }) => {
      await page.getByLabel('Ontvangstdatum*').click({ delay: clickDelaySlow });
      await fillDate({ page, fieldLocator: '//input[contains(@type,"date")]' });
    });

    test('Fill Extern kenmerk', async ({ }) => {
      await page.getByLabel('Extern kenmerk*').fill('Het externe kenmerk');
    });

    test('Select Inhoudelijke behandeling', async ({ }) => {
      await page.getByLabel('Inhoudelijke behandeling').check();
    });

    test('Select Raad', async ({ }) => {
      await page.getByLabel('Raad').check();
    });

    test('Select Gemeentesecretaris', async ({ }) => {
      await page.getByLabel('Gemeentesecretaris').check();
    });

    test('Select Geregistreerde aanvrager', async ({ }) => {
      await page.getByLabel('Geregistreerde aanvrager').check();
    });

    test('Go to the overview page', async ({ }) => {
      await page.getByRole('button', { name: 'Volgende' }).click();
    });

    test('Check text Onderwerp', async ({ }) => {
      await expect(page.getByText(`Het onderwerp op ${dateTimeStamp}`)).toBeVisible();
    });

    test('Check text Taakveld', async ({ }) => {
      await expect(page.getByText('Inwoners')).toBeVisible();
    });

    test('Check text Samenvatting', async ({ }) => {
      await expect(page.getByText('De samenvatting van deze zaak van zaaktype Basiszaaktype dun')).toBeVisible();
    });

    test('Check text Locatie', async ({ }) => {
      await expect(page.getByText('Buddingh\'plein 22, 3311BV Dordrecht')).toBeVisible();
    });

    test('Check text Ontvangstdatum', async ({ }) => {
      await page.getByText('Ontvangstdatum').scrollIntoViewIfNeeded();
      await checkDateShown({ fieldLocator: page.locator(locators.labelLeftOnOverview('Ontvangstdatum')) });
    });

    test('Check text Extern kenmerk', async ({ }) => {
      await page.getByText('Extern kenmerk').scrollIntoViewIfNeeded();
      await expect(page.getByText('Het externe kenmerk')).toBeVisible();
    });

    test('Check text Behandeltype', async ({ }) => {
      await page.getByText('Behandeltype').scrollIntoViewIfNeeded();
      await expect(page.getByText('Inhoudelijke behandeling')).toBeVisible();
    });

    test('Check text Kopiehouders voor deze zaak', async ({ }) => {
      await page.getByText('Kopiehouders voor deze zaak').scrollIntoViewIfNeeded();
      await expect(page.getByText('Raad')).toBeVisible();
      await expect(page.getByText('Gemeentesecretaris')).toBeVisible();
    });

    test('Check text Wie is de contactpersoon voor deze zaak?', async ({ }) => {
      await page.getByText('Wie is de contactpersoon voor deze zaak?').scrollIntoViewIfNeeded();
      await expect(page.getByText('Geregistreerde aanvrager')).toBeVisible();
    });

    test('Check optie Automatisch een ontvangstbevestiging maken?', async ({ }) => {
      await page.getByText('Automatisch een ontvangstbevestiging maken?').scrollIntoViewIfNeeded();
      await expect(page.locator(locators.checkNothingSelected('Automatisch een ontvangstbevestiging maken?'))).toBeVisible();
    });

    test('Set Toewijzing', async ({ }) => {
      await page.locator('//li[@data-name="me"]/button//zs-icon').click();
      await page.getByLabel('Ook afdeling wijzigen').check();
    });

    test('Make the case', async ({ }) => {
      await page.getByRole('button', { name: 'Versturen' }).click();
      await page.waitForLoadState('domcontentloaded');
      await page.waitForLoadState('networkidle');
    });

    test('Check the message the case is being made message is shown', async ({ }) => {
      await expect(page.locator(
        '//div[@class="snack-message" and .//text()["Uw zaak wordt geregistreerd."]]'
      )).toBeVisible({ timeout: superLongTimeout });
    });

    test('Check the message the case is made message is shown', async ({ }) => {
      const snackMessage = page.locator(
        '//div[@class="snack-message" and .//text()[starts-with(.,"Zaak is door u in behandeling genomen onder zaaknummer ")]]'
      );
      const textEndsWithCase = (await snackMessage.innerText({ timeout: superLongTimeout })).toString();
      caseNumberInMessage = getCaseNumber({ textEndsWithCase });
    });

    test('Check url', async ({ }) => {
      await expect(page).toHaveURL(/.*\/zaak\/.*/, { timeout: extreemLongTimeout });
    });

    test('Get the case number out of the url', async ({ }) => {
      caseNumberInUrl = getCaseNumber({ textEndsWithCase: page.url(), seperator: '\/' });
    });

    test('Check the case number out of the url is same as in message', async ({ }) => {
      expect(caseNumberInMessage === caseNumberInUrl).toBeTruthy();
    });

    test('Check Afhandelen zaak is shown', async ({ }) => {
      await expect(page.getByText('Afhandelen zaak')).toBeVisible();
    });

    test('Add remark about finishing the case', async ({ }) => {
      await page.getByRole('listitem').filter({ hasText: 'Opmerkingen over de afhandeling* Veld toevoegen' }
      ).getByRole('textbox').click();
      await page.getByRole('listitem').filter({ hasText: 'Opmerkingen over de afhandeling* Veld toevoegen' }
      ).getByRole('textbox').fill('Dit is de opmerking bij de afhandeling!');
    });

    test('Check that all information and documents have been checked', async ({ }) => {
      await page.locator('label').filter({ hasText: 'Ja' }).scrollIntoViewIfNeeded();
      await page.locator('label').filter({ hasText: 'Ja' }).click();
    });

    test('Set the text Archieftoelichting', async ({ }) => {
      await page.getByRole('listitem').filter({ hasText: 'Archieftoelichting* Veld toevoegen' }).getByRole('textbox').click();
      await page.getByRole('listitem').filter({ hasText: 'Archieftoelichting* Veld toevoegen' }).getByRole('textbox').fill(
        'Deze zaak mag zo snel mogelijk vernietigd worden');
    });

    test('Check the option Proces afgehandeld', async ({ }) => {
      await page.getByLabel('Proces afgehandeld').check();
    });

    test('Finish the case processing', async ({ }) => {
      await page.getByRole('button', { name: ' Zaak afhandelen' }).click();
    });

    test('Check message De zaak is succesvol afgerond', async ({ }) => {
      await expect(page.getByText('De zaak is succesvol afgerond.')).toBeVisible({ timeout: superLongTimeout });
      await expect(page.getByText('De zaak is succesvol afgerond.')).toBeHidden({ timeout: longTimeout });
    });

    test('Check the case is not shown anymore on the task list', async ({ }) => {
      await checkTextReload({ page, textToToCheck: caseNumberInMessage, visible: false });
    });

  });
}