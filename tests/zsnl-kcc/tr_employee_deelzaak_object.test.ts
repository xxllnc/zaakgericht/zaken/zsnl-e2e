/* eslint-disable camelcase */
import { BrowserContext, Page, expect, test } from '@playwright/test';
import { baseUrl, autobehandelaarUser } from '../../e2e_tests';
import {
  logOff, openSystemAndLogin, createNewCase,
  fillRegistrationPage,
  registerCaseAndCheckMessages,
  checkObject,
} from '../../partial-tests';
import { TestrondeDataArray, testrondeDataArray } from '../../testfiles/TESTRONDE/testrondeTestdata';
import { adjustOnPageTestDataArray, adjustTestDataArrayToScript } from '../../utils';
import { zsnlTestConfig } from '../../playwright.config';
import { personDataDev, personDataDevelopment } from '../../testfiles/personData';

const { options, shortTimeout, longTimeout } = zsnlTestConfig;
const systemUrl = `${baseUrl}`;
const system = `${baseUrl
  .replace('https://', '')
  .split('.')[0]}` === 'dev' ? 'dev' : 'development';
const personData = system === 'dev' ? [...personDataDev] : [...personDataDevelopment];
const BSNContact = '547608238';
const dataIndex = personData.map((el) => el.BSN).indexOf(BSNContact);
const contact = `${personData[dataIndex].insertions} ${personData[dataIndex].familyName}`;

let caseNumberInUrl = '';
const casetypeMain = 'Auto TESTRONDE V2';
const casetypePartialCase = 'Auto TESTRONDE deelzaak V2';
const choicesK1016 = [
  'Nee',
  'Ja',
];
const testrondeDataArrayScript: { [key: string]: TestrondeDataArray } = {
  auto_tst_k1007_rekeningnummer: {
    overviewValue: 'NL91ABNA0417164300',
  },
  auto_tst_k1010_email: {
    overviewValue: 'autobehandelaar@xxllnc.nl',
  },
  auto_tst_k1015_numeriek: {
    overviewValue: '10',
  },
  auto_tst_k1016_enkelvoudige_keuze: {
    overviewValue: 'Ja', checkValue: 'Ja'
  },
  auto_tst_k1018_rich_text: {
    overviewValue: '<p><strong>BOLD</strong></p><ol><li>lijst 1</li></ol><ul><li>BULLET 1</li></ul><p><em>Italic</em></p>',
  },
  auto_tst_k1019_tekstveld: {
    overviewValue: 'Zaaksysteem.nl',
  },
  auto_tst_k1021_groot_tekstveld: {
    overviewValue: '100%&nbsp;zaakgericht werken Voor vernieuwers binnen de overheid die ambities waar willen maken! ',
  },
  auto_tst_k1022_webadres: {
    overviewValue: 'https://zaaksysteem.nl',
  },
  auto_tst_k1023_valuta: {
    overviewValue: '20,00',
  },
};
const onPageName = 'onObject'
const testrondeDataArrayOnpage = [
  'auto_tst_o0001_object_naam', 'auto_tst_k0000_datum_test',
  'auto_tst_k1000_datum_release_tag',
  'auto_tst_k1001_v2_adres', 'auto_tst_k1007_rekeningnummer', 'auto_tst_k1008_meervoudige_keuze',
  'auto_tst_k1009_datum', 'auto_tst_k1010_email', 'auto_tst_k1011_document',
  'auto_tst_k1011_document_2', 'auto_tst_k1011_document_3', 'auto_tst_k1015_numeriek',
  'auto_tst_k1016_enkelvoudige_keuze', 'auto_tst_k1017_keuzelijst',
  'auto_tst_k1018_rich_text', 'auto_tst_k1019_tekstveld',
  'auto_tst_k1021_groot_tekstveld', 'auto_tst_k1022_webadres', 'auto_tst_k1023_valuta'
];

Object.values(choicesK1016).forEach(choiceK1016 => {
  test.describe.serial(`TESTRONDE employee creates and checks object in deelzaak choice "${choiceK1016}"`, async () => {
    let page: Page;
    let context: BrowserContext;

    test.beforeAll(async ({ browser }) => {
      context = await browser.newContext(options);
      page = await context.newPage();
      await openSystemAndLogin({
        page,
        systemUrl,
        user: autobehandelaarUser
      });
    });

    test.afterAll(async () => {
      await logOff({ page, systemUrl });
      await context.close();
    });

    test(`Start with a new case ${casetypeMain} for choice "${choiceK1016}"`, async ({ }) => {
      await createNewCase({ page, caseType: casetypeMain });
    });

    test(`Set the reference values for ${casetypeMain} for choice "${choiceK1016}"`, async ({ }, testInfo) => {
      test.skip(choiceK1016 === 'Nee');
      adjustTestDataArrayToScript({ scriptTestDataArray: testrondeDataArrayScript, testScriptName: testInfo.title });
    });

    test(`Adjust the OnPages values for ${casetypeMain} for choice "${choiceK1016}"`, async ({ }, testInfo) => {
      adjustOnPageTestDataArray({ attributeArray: testrondeDataArrayOnpage, onPageName: onPageName, testScriptName: testInfo.title });
    });

    test(`Start the registration for case ${casetypeMain} for choice "${choiceK1016}"`, async ({ }, testInfo) => {
      await fillRegistrationPage({ page, onPage: 'form1', testScriptName: testInfo.title });
    });

    test(`Set the choice for (not)activating rules for case ${casetypeMain} for choice "${choiceK1016}"`, async ({ }) => {
      await page.getByText('k1016').scrollIntoViewIfNeeded();
      choiceK1016 === 'Ja' ?
        await page.getByLabel('Ja').check() :
        await page.getByLabel('Nee').check();
    });

    test(`Fill the rest of the fields for case ${casetypeMain} for choice "${choiceK1016}"`, async ({ }, testInfo) => {
      await fillRegistrationPage({ page, onPage: 'form2', testScriptName: testInfo.title });
    });

    test(`Register the case ${casetypeMain} for choice "${choiceK1016}"`, async ({ }, testInfo) => {
      caseNumberInUrl = await registerCaseAndCheckMessages({ page, waitForText: 'Extra testen', testScriptName: testInfo.title });
    });

    test(`Go to the relation page on case ${casetypeMain} for choice "${choiceK1016}"`, async ({ }, testInfo) => {
      await page.getByRole('link', { name: 'Relaties' }).click();
    });

    test(`Wait for case ${casetypePartialCase} for choice "${choiceK1016}"`, async ({ }, testInfo) => {
      await expect.poll(async () => {
        try {
          await expect(page.frameLocator('#react-iframe').getByText(`${casetypePartialCase}`)).toBeVisible({ timeout: shortTimeout });
        }
        catch {
          await page.reload({ waitUntil: 'domcontentloaded' });
          await page.waitForLoadState('networkidle');
        }
        return page.frameLocator('#react-iframe').getByText(`${casetypePartialCase}`).isVisible();
      }, {
        // Probe, wait 1s, probe, wait 2s, probe, wait 5s, probe, wait 5s, probe, .... Defaults to [100, 250, 500, 1000].
        // intervals: [3_000, 2_000, 2_000],
        timeout: 40_000
      }).toBeTruthy();
    });

    test(`Open partial case ${casetypePartialCase} for choice "${choiceK1016}"`, async ({ },) => {
      await page.frameLocator('#react-iframe').getByRole('row').filter({ hasText: `${casetypePartialCase}` }).getByRole('link').click();
    });

    test(`Create object for case ${casetypePartialCase} for choice "${choiceK1016}"`, async ({ }, testInfo) => {
      await test.step(`Start making object for case ${casetypePartialCase} was registered message for "${choiceK1016}"`, async () => {
        await page.getByRole('button', { name: 'Object V2 aanmaken via relatiekenmerk' }).click();
      });

      await test.step(`Add unique timestamp for object for case ${casetypePartialCase} was registered message for "${choiceK1016}"`, async () => {
        await page.frameLocator('#react-iframe').locator('input[name="auto_tst_o0001_object_naam"]').click();
        await page.frameLocator('#react-iframe').locator('input[name="auto_tst_o0001_object_naam"]').press('Control+a');
        await page.frameLocator('#react-iframe').locator('input[name="auto_tst_o0001_object_naam"]').fill(`${testrondeDataArray.auto_tst_o0001_object_naam.fillValue}`);
      });

      await test.step(`Check referenced fields in object for case ${casetypePartialCase} was registered message for "${choiceK1016}"`, async () => {
        await checkObject({
          page, onPage: onPageName,
          testScriptName: testInfo.title,
          modeShowOrEditObject: 'editObject'
        })
      });

      await test.step(`Add geolocation in object for case ${casetypePartialCase} was registered message for "${choiceK1016}"`, async () => {
        await page.frameLocator('#react-iframe').getByRole('heading', { name: 'Geolocatie is de titel' }).scrollIntoViewIfNeeded();
        await page.frameLocator('#react-iframe').frameLocator('iframe[title="auto_tst_k1002_v2_geolocatie"]').getByRole('link', { name: 'Draw a rectangle' }).click();
        await page.frameLocator('#react-iframe').getByTitle('auto_tst_k1002_v2_geolocatie').hover({ position: { x: 350, y: 140 } });
        await page.mouse.down();
        await page.frameLocator('#react-iframe').getByTitle('auto_tst_k1002_v2_geolocatie').hover({ position: { x: 450, y: 240 } });
        await page.mouse.up();
      });

      await test.step(`Make object for case ${casetypePartialCase} was registered message for "${choiceK1016}"`, async () => {
        await page.frameLocator('#react-iframe').getByRole('button', { name: 'Aanmaken' }).click();
      });
    });

    test(`Check object for case ${casetypePartialCase} for choice "${choiceK1016}"`, async ({ }, testInfo) => {
      await page.getByText('Object openen').click();
      await checkObject({
        page: page, onPage: onPageName,
        testScriptName: testInfo.title, modeShowOrEditObject: 'showObject'
      })
      await page.goBack({ waitUntil: 'domcontentloaded' });
    });

    test(`Add contact ${casetypePartialCase} for choice "${choiceK1016}"`, async ({ }, testInfo) => {
      await page.getByLabel('auto_relatie_kenmerk_testronde_contact*').click();
      await page.getByLabel('auto_relatie_kenmerk_testronde_contact*').fill(BSNContact);
      await page.getByRole('button', { name: `${contact}` }).click();
    });

    test(`Finish partial case ${casetypePartialCase} for choice "${choiceK1016}"`, async ({ }, testInfo) => {
      await page.getByRole('button', { name: ' Zaak afhandelen' }).click();
      await expect(page.getByText('Mijn openstaande zaken')).toBeVisible();
    });

    test(`Finish main case ${casetypeMain} for choice "${choiceK1016}"`, async ({ }, testInfo) => {
      await page.getByRole('row', { name: 'Zaak: ' + caseNumberInUrl }).click({ timeout: longTimeout });
      await page.getByRole('link', { name: 'Open tab Taken' }).first().click();
      await page.frameLocator('#react-iframe').locator('button[name="taskListDone"]').click();
      await page.reload();
      const smallScreen = await page.locator('zs-case-phase-sidebar').getByRole('button', { name: '' }).isVisible();
      smallScreen ?
        await page.locator('zs-case-phase-sidebar').getByRole('button', { name: '' }).click() : '';
      await page.getByRole('button', { name: ' Fase afronden' }).click();
      await expect(page.locator('//div[@class="snack-message"]')
        .filter({ hasText: 'De fase is succesvol afgerond' }).first()
      ).toBeVisible();
      await page.getByRole('button', { name: 'Melding sluiten' }).click();
      await page.getByRole('button', { name: ' Zaak afhandelen' }).click();
      await expect(page.locator('//div[@class="snack-message"]')
        .filter({ hasText: 'De zaak is succesvol afgerond' }).first()
      ).toBeVisible();
      await page.getByRole('button', { name: 'Melding sluiten' }).click();
    });
  });
});