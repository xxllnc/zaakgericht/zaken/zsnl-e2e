/* eslint-disable camelcase */
import { BrowserContext, Page, expect, test } from '@playwright/test';
import { baseUrl, behandelaarUser } from '../../e2e_tests';
import {
  logOff, openSystemAndLogin, createNewCase,
  checkDefaultsRegistrationPage, fillRegistrationPage,
  checkChoiceIsYesRegistrationPage,
  exitForm,
  checkOverviewPage
} from '../../partial-tests';
import { TestrondeDataArray, } from '../../testfiles/TESTRONDE/testrondeTestdata';
import { adjustTestDataArrayToScript } from '../../utils';
import { zsnlTestConfig } from '../../playwright.config';

const { options, } = zsnlTestConfig;
const systemUrl = `${baseUrl}`;
const casetypeMain = 'Auto TESTRONDE V2';
const choicesK1016 = [
  'Nee',
  'Ja',
];
const testrondeDataArrayScript: { [key: string]: TestrondeDataArray } = {
  auto_tst_k1007_rekeningnummer: {
    overviewValue: 'NL91ABNA0417164300',
  },
  auto_tst_k1010_email: {
    overviewValue: 'autobehandelaar@xxllnc.nl',
  },
  auto_tst_k1015_numeriek: {
    overviewValue: '10',
  },
  auto_tst_k1016_enkelvoudige_keuze: {
    overviewValue: 'Ja', checkValue: 'Ja'
  },
  auto_tst_k1018_rich_text: {
    overviewValue: 'BOLD lijst 1 BULLET 1 Italic',
  },
  auto_tst_k1019_tekstveld: {
    overviewValue: 'Zaaksysteem.nl',
  },
  auto_tst_k1021_groot_tekstveld: {
    overviewValue: '100% zaakgericht werken Voor vernieuwers binnen de overheid die ambities waar willen maken!123456',
  },
  auto_tst_k1022_webadres: {
    overviewValue: 'https://zaaksysteem.nl',
  },
  auto_tst_k1023_valuta: {
    overviewValue: '20,00',
  },
};

Object.values(choicesK1016).forEach(choiceK1016 => {
  test.describe.serial(`TESTRONDE employee fill in and check of form for choice "${choiceK1016}" @inStartE2E`, async () => {
    let page: Page;
    let context: BrowserContext;

    test.beforeAll(async ({ browser }) => {
      context = await browser.newContext(options);
      page = await context.newPage();
      await openSystemAndLogin({
        page,
        systemUrl,
        user: behandelaarUser
      });
    });

    test.afterAll(async () => {
      await logOff({ page, systemUrl });
      await context.close();
    });

    test(`Start with a new case ${casetypeMain} for choice "${choiceK1016}" @inStartE2E`, async ({ }) => {
      await createNewCase({ page, caseType: casetypeMain });
    });

    test(`Set the values for ${casetypeMain} for choice "${choiceK1016}" @inStartE2E`, async ({ }, testInfo) => {
      // eslint-disable-next-line playwright/no-skipped-test
      test.skip(choiceK1016 === 'Nee');
      adjustTestDataArrayToScript({ scriptTestDataArray: testrondeDataArrayScript, testScriptName: testInfo.title });
    });

    test(`Check the default values on page 1 for case ${casetypeMain} for choice "${choiceK1016}" @inStartE2E`, async ({ }, testInfo) => {
      await checkDefaultsRegistrationPage({ page, onPage: 'form1', testScriptName: testInfo.title });
    });

    test(`Start the registration for case ${casetypeMain} for choice "${choiceK1016}" @inStartE2E`, async ({ }, testInfo) => {
      await fillRegistrationPage({ page, onPage: 'form1', testScriptName: testInfo.title });
    });

    test(`Check the text block visibility before rules for case ${casetypeMain} for choice "${choiceK1016}" @inStartE2E`, async ({ }) => {
      await page.getByText('3311BV Meer Veld toevoegen').click();
      await expect(page.getByText('1114AD 1051JLH.J.E. Wenckebachweg 90Donker Curtiusstraat 7 Meer Veld toevoegen')).toBeVisible();
    });

    test(`Check the default values on page 2 for case ${casetypeMain} for choice "${choiceK1016}" @inStartE2E`, async ({ }, testInfo) => {
      await checkDefaultsRegistrationPage({ page, onPage: 'form2', testScriptName: testInfo.title });
    });

    test(`Set the choice for (not)activating rules for case ${casetypeMain} for choice "${choiceK1016}" @inStartE2E`, async ({ }) => {
      await page.getByText('k1016').scrollIntoViewIfNeeded();
      // eslint-disable-next-line playwright/no-conditional-in-test
      choiceK1016 === 'Ja' ?
        await page.getByLabel('Ja').check() :
        await page.getByLabel('Nee').check();
    });

    test(`Check the text block (non)visibility after rules for case ${casetypeMain} for choice "${choiceK1016}" @inStartE2E`, async ({ }) => {
      await page.getByText('3311BV Meer Veld toevoegen').click();
      // eslint-disable-next-line playwright/no-conditional-in-test
      choiceK1016 === 'Ja' ?
        await expect(page.getByText('1114AD 1051JLH.J.E. Wenckebachweg 90Donker Curtiusstraat 7 Meer Veld toevoegen')).toBeHidden() :
        await expect(page.getByText('1114AD 1051JLH.J.E. Wenckebachweg 90Donker Curtiusstraat 7 Meer Veld toevoegen')).toBeVisible();
    });

    test(`Check the fillings after rules for case ${casetypeMain} for choice "${choiceK1016}" @inStartE2E`, async ({ }, testInfo) => {
      // eslint-disable-next-line playwright/no-conditional-in-test
      choiceK1016 === 'Ja' ?
        await checkChoiceIsYesRegistrationPage({ page, onPage: 'form2', testScriptName: testInfo.title }) :
        await checkDefaultsRegistrationPage({ page, onPage: 'form2', testScriptName: testInfo.title });
    });

    test(`Fill the rest of the fields for case ${casetypeMain} for choice "${choiceK1016}" @inStartE2E`, async ({ }, testInfo) => {
      await fillRegistrationPage({ page, onPage: 'form2', testScriptName: testInfo.title });
    });

    test(`Check the overview page ${casetypeMain} for choice "${choiceK1016}" @inStartE2E`, async ({ }, testInfo) => {
      await checkOverviewPage({ page, onPage: 'overview', testScriptName: testInfo.title });
    });

    test(`Stop the application for case ${casetypeMain} for choice "${choiceK1016}" @inStartE2E`, async ({ }, testInfo) => {
      await exitForm({ page, testScriptName: testInfo.title });
    });
  });
});