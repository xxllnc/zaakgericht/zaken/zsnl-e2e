// import { test as base } from '@playwright/test'
import dotenv from 'dotenv';
import { zsnlTestConfig } from './playwright.config';
import { LoginUser } from './types';
// import { test as base } from '@playwright/test'

// Read from default ".env" file.
dotenv.config();

export const beheerderUser: LoginUser = {
  name: process.env?.BEHEERDER_USER_NAME ?? zsnlTestConfig.beheerderUser,
  password: process.env?.BEHEERDER_USER_PASSWORD ?? zsnlTestConfig.beheerderPassword
};

export const adminUser: LoginUser = {
  name: process.env?.ADMIN_USER_NAME ?? zsnlTestConfig.adminUser,
  password: process.env?.ADMIN_USER_PASSWORD ?? zsnlTestConfig.adminPassword
};

export const behandelaarUser: LoginUser = {
  name: process.env?.BEHANDELAAR_USER_NAME ?? zsnlTestConfig.behandelaarUser,
  password: process.env?.BEHANDELAAR_USER_PASSWORD ?? zsnlTestConfig.behandelaarPassword
};

export const autobehandelaarUser: LoginUser = {
  name: process.env?.BEHANDELAAR_USER_NAME ?? zsnlTestConfig.autobehandelaarUser,
  password: process.env?.BEHANDELAAR_USER_PASSWORD ?? zsnlTestConfig.autobehandelaarPassword
};

export const ogoneSha = process.env?.OGONE_SHA ?? 'You need to fill this in .env';

const baseDomain = process.env?.BASE_DOMAIN ?? zsnlTestConfig.baseDomain;

export const baseUrl = `https://${baseDomain}/`;
export const pageTitle = process.env?.PAGE_TITLE ?? zsnlTestConfig.pageTitle;

console.log({ baseDomain, baseUrl, pageTitle });