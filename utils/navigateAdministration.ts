import { Page } from '@playwright/test';

interface Props {
  page: Page
  menuItem: string,
}

export const navigateAdministration = async ({ page, menuItem }: Props) => {
  await page.getByRole('button', { name: 'Hoofdmenu openen' }).click();
  await page.getByRole('menuitem', { name: ' Catalogus' }).click();
  menuItem !== 'Catalogus' ? await page.getByRole('link', { name: menuItem }).click() : '';
  await page.waitForLoadState('domcontentloaded');
  await page.waitForLoadState('networkidle');
};