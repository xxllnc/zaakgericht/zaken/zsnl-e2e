interface Props {
  textEndsWithCase: string,
  seperator?: string
}

export const getCaseNumber = ({
  textEndsWithCase, seperator = ' ' }: Props) => {

  const textArray = textEndsWithCase.split(new RegExp(`${seperator}`));
  const caseNumber = textArray[textArray.length - 1];
  console.log(`The case number is: "${caseNumber}"`);

  return caseNumber;
};