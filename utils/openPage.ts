import { Page } from '@playwright/test';

export const openPage = async (page: Page, url: string) => {
  await page.goto(url.toString(), { waitUntil: 'domcontentloaded' });
  await page.waitForLoadState('networkidle');
};