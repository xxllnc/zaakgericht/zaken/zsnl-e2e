export const locators = {
  persoonLink: (textToReplace: string) => `//td[.//text()="${textToReplace}"]//parent::tr//a[.//text()="Persoon"]`,
  text: (text: string) => `text=${text}`,
  labelLeftRichTextFatt: (testToReplace: string) => `//label[contains(.//text(),"${testToReplace}")]/ancestor::vorm-field//div/p`,
  labelLeftTextInContact: (textToReplace: string) => `(//div[contains(.//text(),"${textToReplace}")]/following::div//div)[1]`,
  labeledField: (textToReplace: string, hasVormField = false) =>
    `//label[.//text()="${textToReplace}"]/parent::div/parent::${hasVormField ? 'vorm-field' : 'div'}/div[2]`,
  objectInputField: (textToReplace: string, inputType = '') =>
    `//h6[.//text()="${textToReplace}"]/parent::div/parent::div/div[2]${inputType === 'Document' ?
      '//p' : inputType === 'Adres V2' ? '/div/div/input' : inputType === 'Groot tekstveld' ? '//textarea' :
        inputType === 'Keuzelijst' || inputType === 'Valuta' ? '//input' : ''}`,
  objectShowField: (textToReplace: string, inputType = '') =>
    `//h6[.//text()="${textToReplace}"]/parent::div/parent::div/parent::div/div[2]${inputType === 'Document' ?
      '//p' : inputType === 'Groot tekstveld' ? '//textarea' : ''}`,
  labelLeftOnPersonFormOverview: (textToReplace: string, numberOfOccurence: string) =>
    `(//*[contains(.//text(),"${textToReplace}")]/parent::div/parent::div/parent::div//div[2]/div)[${numberOfOccurence}]`,
  labelLeftOnOverview: (textToReplace: string) => (`//*[contains(.//text(), "${textToReplace}")]/ancestor::li//ul//span`),
  resultFound: (textToReplace: string) => `(//*[contains(@class,"suggestion") and contains(.//text(),"${textToReplace}")])[1]`,
  checkNothingSelected: (textToReplace: string) =>
    `//*[contains(.//text(),'${textToReplace}')]//ancestor::li//ul[contains(@class,'empty')]`,
  partialTextLocator: (textToReplace: string) => `(//*[contains(.//text(),"${textToReplace}")])[1]`,
};