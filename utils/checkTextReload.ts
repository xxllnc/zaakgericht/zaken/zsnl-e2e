import { expect, Page } from '@playwright/test';
import { zsnlTestConfig } from '../playwright.config';

const { shortTimeout, longTimeout } = zsnlTestConfig;

type Props = {
  page: Page
  textToToCheck: string
  visible: boolean
}
export const checkTextReload = async ({ page, textToToCheck, visible }: Props) => {
  await expect.poll(async () => {

    try {
      visible ?
        await expect(page.getByText(textToToCheck)).toBeVisible({ timeout: longTimeout }) :
        await expect(page.getByText(textToToCheck)).toBeHidden({ timeout: shortTimeout });
      return true;
    }
    catch {
      try {
        await expect(page.getByText('Er is een nieuwe versie van Zaaksysteem beschikbaar')).toBeVisible({ timeout: shortTimeout });
        await page.getByRole('button', { name: 'Melding sluiten' }).click();
      }
      catch {
        //Just continue
      }
      await page.reload({ waitUntil: 'domcontentloaded' });
      return false;
    }
  }, {
    // Probe, wait 2s, probe, wait 2s, probe, wait 1s, probe, wait 1s, probe, .... Defaults to [100, 250, 500, 1000].
    intervals: [2_000, 2_000, 1_000],
    timeout: 20_000
  }).toBeTruthy();
};