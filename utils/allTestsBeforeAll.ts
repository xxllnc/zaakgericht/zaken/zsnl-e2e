import { Browser, BrowserContext, Page, TestInfo } from '@playwright/test';
import { zsnlTestConfig } from '../playwright.config';

const { options } = zsnlTestConfig;

export async function allTestsBeforeAll(context: BrowserContext, browser: Browser,
  page: Page, testInfo: TestInfo) {
  context = await browser.newContext(options);
  page = await context.newPage();

  // Log and continue all network requests
  await page.route('/api/**', (route, request) => {
    console.log(`${testInfo.title} request: ${request.url()} timing: ${request.timing} `);
    route.continue();
  });
  return { context, page };
}
