import { Page } from '@playwright/test';
import { format } from 'date-fns';

interface Props {
  page: Page,
  fieldLocator: string,
  dateToFill?: string
}

export const fillDate = async ({ page, fieldLocator, dateToFill = format(new Date(), 'yyyy-MM-dd') }: Props) => {
  await page.locator(fieldLocator).click();
  await page.locator(fieldLocator).fill(`${dateToFill}`);
  const dateFilledIn = await page.locator('//input[contains(@type,"date")]').inputValue();
  console.log(`the date it sshould be: "${dateToFill}" and the date that was filled in: "${dateFilledIn}"`);
};