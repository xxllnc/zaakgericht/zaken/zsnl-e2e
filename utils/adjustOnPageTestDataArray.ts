import { Page, expect, test } from '@playwright/test';
import { testrondeDataArray } from '../testfiles/TESTRONDE/testrondeTestdata';

interface Props {
  attributeArray: string[]
  onPageName: string
  testScriptName: string
}

export const adjustOnPageTestDataArray = async ({
  attributeArray,
  onPageName,
  testScriptName
}: Props) => {
  attributeArray.forEach(function x(attributeName) {
    if (attributeName in testrondeDataArray) {
      test.step(`Add the onPage in testrondeDataArray for '+
      '${attributeName} for ${testScriptName}`, async () => {
        testrondeDataArray[attributeName].onPages !== undefined ?
          testrondeDataArray[attributeName].onPages?.push(onPageName) :
          testrondeDataArray[attributeName].onPages = [onPageName];
      })
    };
  })
};
