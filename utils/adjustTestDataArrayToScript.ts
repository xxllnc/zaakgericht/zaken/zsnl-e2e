import { Page, expect, test } from '@playwright/test';
import { TestrondeDataArray, testrondeDataArray } from '../testfiles/TESTRONDE/testrondeTestdata';

interface Props {
  scriptTestDataArray: TestrondeDataArray
  testScriptName: string
}

export const adjustTestDataArrayToScript = async ({
  scriptTestDataArray,
  testScriptName
}: Props) => {
  for (const key in scriptTestDataArray) {
    if (key in testrondeDataArray) {
      for (const attribute in scriptTestDataArray[key]) {
        if (attribute !== undefined) {
          await test.step(`Overwrite the testrondeDataArray value for '+
        '${scriptTestDataArray[key][attribute]} for ${testScriptName}`, async () => {
            testrondeDataArray[key][attribute] = scriptTestDataArray[key][attribute];
          });
        }
      }
    }
  }
};
