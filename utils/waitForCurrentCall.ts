import { Page } from '@playwright/test';
import { baseUrl } from '../e2e_tests';

const systemUrl = `${baseUrl}`;

interface Props {
  page: Page,
}

export const waitForCurrentCall = async ({
  page }: Props) => {
  await page.waitForRequest(`${systemUrl}api/v1/session/current`);
  await page.waitForResponse(res => res.status() === 200);
};
