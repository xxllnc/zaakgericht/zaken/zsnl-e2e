import { expect, Locator } from '@playwright/test';
import { format } from 'date-fns';
import { zsnlTestConfig } from '../playwright.config';
const { shortTimeout } = zsnlTestConfig;
interface Props {
  fieldLocator: Locator,
}

const recieveDate1 = format(new Date(), 'MM-dd-yyyy');
const recieveDate2 = format(new Date(), 'dd-MM-yyyy');

export const checkDateShown = async ({
  fieldLocator }: Props) => {

  await fieldLocator.scrollIntoViewIfNeeded();
  try {
    await expect(fieldLocator).toHaveText(recieveDate1, { timeout: shortTimeout });
  }
  catch {
    await expect(fieldLocator).toHaveText(recieveDate2);
  }
};