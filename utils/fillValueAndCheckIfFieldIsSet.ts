import { expect, Locator } from '@playwright/test';
import { zsnlTestConfig } from '../playwright.config';

interface Props {
  fieldLocator: Locator,
  value: string,
  expectedValue?: string,
  fieldLocatorValue?: Locator
}

export const fillValueAndCheckIfFieldIsSet = async ({
  fieldLocator, value, expectedValue = value, fieldLocatorValue = fieldLocator }: Props) => {

  await fieldLocator.click({ delay: zsnlTestConfig.clickDelay });
  await fieldLocator.press('Control+a');
  await fieldLocator.fill(value);
  await expect(fieldLocatorValue).toHaveValue(expectedValue);
};