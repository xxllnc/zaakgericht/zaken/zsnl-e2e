export interface PersonData {
  BSN: string
  firstNames: string
  insertions: string
  familyName: string
  nobleTitle: string
  birthDate: string
  gender: string
  residenceCountry: string
  residenceStreet: string
  residenceZipcode: string
  residenceHouseNumber: string
  residenceHouseNumberLetter: string
  residenceHouseNumberSuffix: string
  residenceCity: string
  foreignAddress1: string
  foreignAddress2: string
  foreignAddress3: string
  addressInCommunity: boolean | string
  correspondenceStreet: string
  correspondenceZipcode: string
  correspondenceHouseNumber: string
  correspondenceHouseLletter: string
  correspondenceHouseNumberSuffix: string
  correspondenceCity: string
  correspondenceCountry: string
  correspondenceForeignAddress1: string
  correspondenceForeignAddress2: string
  correspondenceForeignAddress3: string
  phoneNumber: string
  mobileNumber: string
  email: string
  preferenceChannel: string
  internalNote: string
}

export const personDataDev: PersonData[] = [
  {
    BSN: '132915947',
    firstNames: 'Ŗî Ãō Øū Ŋÿ Ği ŢžŰŲ ŜŞőĠĪ Ŷŵ Ĉŷ',
    insertions: 'over \'t',
    familyName: 'T.Śar ŃĆ ĹāÑ ŤÙmön ĊéŴÀŅŇĩ Ļl\'ÁÚŘŠĎÉ Pomme- d\' Or ĽÒÓĢÛŨ',
    nobleTitle: 'Bardon',
    birthDate: '01-01-2010',
    gender: 'Man',
    residenceCountry: 'Nederland',
    residenceStreet: 'Yhkqlkrfgmborzyuk',
    residenceZipcode: '1234AA',
    residenceHouseNumber: '49',
    residenceHouseNumberLetter: '',
    residenceHouseNumberSuffix: 'BU',
    residenceCity: 'Testgemeente',
    foreignAddress1: '',
    foreignAddress2: '',
    foreignAddress3: '',
    addressInCommunity: false,
    correspondenceStreet: '',
    correspondenceZipcode: '',
    correspondenceHouseNumber: '',
    correspondenceHouseLletter: '',
    correspondenceHouseNumberSuffix: '',
    correspondenceCity: '',
    correspondenceCountry: '',
    correspondenceForeignAddress1: '',
    correspondenceForeignAddress2: '',
    correspondenceForeignAddress3: '',
    phoneNumber: '0105123455',
    mobileNumber: '0623456789',
    email: 'burgerdiakriet@xxllnc.nl',
    preferenceChannel: 'persoonlijke Internetpagina',
    internalNote: 'DO NOT USE OR CHANGE!!! This person is used in the automated e2e test.'
  },
  {
    BSN: '547608238',
    firstNames: 'Françoise Automatische Regressietest',
    insertions: 'sûr \'t',
    familyName: 'Brăiloiu',
    nobleTitle: '',
    birthDate: '18-06-1980',
    gender: 'Vrouw',
    residenceCountry: 'België',
    residenceStreet: '',
    residenceZipcode: '',
    residenceHouseNumber: '',
    residenceHouseNumberLetter: '',
    residenceHouseNumberSuffix: '',
    residenceCity: '',
    foreignAddress1: 'Rue de Gozée 706',
    foreignAddress2: '6110 Montigny-le-Tilleul',
    foreignAddress3: 'België',
    addressInCommunity: false,
    correspondenceStreet: '',
    correspondenceZipcode: '',
    correspondenceHouseNumber: '',
    correspondenceHouseLletter: '',
    correspondenceHouseNumberSuffix: '',
    correspondenceCity: '',
    correspondenceCountry: '',
    correspondenceForeignAddress1: '',
    correspondenceForeignAddress2: '',
    correspondenceForeignAddress3: '',
    phoneNumber: '0301234567',
    mobileNumber: '0687654321',
    email: 'burgerdiakriet@xxllnc.nl',
    preferenceChannel: 'E-mail',
    internalNote: 'DO NOT USE OR CHANGE!!! This person is used in the automated e2e test.'
  },
  {
    BSN: '010082426',
    firstNames: 'Erika Automatische Regressietest',
    insertions: 'de',
    familyName: 'Goede',
    nobleTitle: 'Gravin',
    birthDate: '01-04-1987',
    gender: 'Anders',
    residenceCountry: 'Nederland',
    residenceStreet: '',
    residenceZipcode: '',
    residenceHouseNumber: '',
    residenceHouseNumberLetter: '',
    residenceHouseNumberSuffix: '',
    residenceCity: '',
    foreignAddress1: '',
    foreignAddress2: '',
    foreignAddress3: '',
    addressInCommunity: true,
    correspondenceStreet: 'Grotemarkt',
    correspondenceZipcode: '3011PA',
    correspondenceHouseNumber: '22',
    correspondenceHouseLletter: '',
    correspondenceHouseNumberSuffix: '',
    correspondenceCity: 'Rotterdam',
    correspondenceCountry: 'Nederland',
    correspondenceForeignAddress1: '',
    correspondenceForeignAddress2: '',
    correspondenceForeignAddress3: '',
    phoneNumber: '0512345667',
    mobileNumber: '0699887700',
    email: 'burgerdiakriet@xxllnc.nl',
    preferenceChannel: 'email',
    internalNote: 'DO NOT USE OR CHANGE!!! This person is used in the automated e2e test.'
  }
];
export const personDataDevelopment: PersonData[] = [
  {
    BSN: '132915947',
    firstNames: 'Jonas',
    insertions: '',
    familyName: 'Kuipers',
    nobleTitle: 'Bardon',
    birthDate: '19-05-1928',
    gender: 'Man',
    residenceCountry: 'Nederland',
    residenceStreet: 'Yhkqlkrfgmborzyuk',
    residenceZipcode: '1234AA',
    residenceHouseNumber: '49',
    residenceHouseNumberLetter: 'Z',
    residenceHouseNumberSuffix: 'BU',
    residenceCity: 'Testgemeente',
    foreignAddress1: 'Rue de Gozée 706',
    foreignAddress2: '6110 Montigny-le-Tilleul',
    foreignAddress3: 'België',
    addressInCommunity: 'true',
    correspondenceStreet: 'Tmrxpypymjuxlivxw',
    correspondenceZipcode: '76',
    correspondenceHouseNumber: 'X',
    correspondenceHouseLletter: 'GZ',
    correspondenceHouseNumberSuffix: '',
    correspondenceCity: 'Testgemeente',
    correspondenceCountry: 'Nederland',
    correspondenceForeignAddress1: '',
    correspondenceForeignAddress2: '',
    correspondenceForeignAddress3: '',
    phoneNumber: '+315123455',
    mobileNumber: '0623456789',
    email: 'burgerdiakriet@xxllnc.nl',
    preferenceChannel: 'telefoon',
    internalNote: 'DO NOT USE OR CHANGE!!! This person is used in the automated e2e test.'
  },
  {
    BSN: '547608238',
    firstNames: 'Jan',
    insertions: '',
    familyName: 'Janssen',
    nobleTitle: 'Bardon',
    birthDate: '10-05-1954',
    gender: 'Man',
    residenceCountry: '',
    residenceStreet: '',
    residenceZipcode: '',
    residenceHouseNumber: '',
    residenceHouseNumberLetter: '',
    residenceHouseNumberSuffix: '',
    residenceCity: '',
    foreignAddress1: '',
    foreignAddress2: '',
    foreignAddress3: '',
    addressInCommunity: 'true',
    correspondenceStreet: 'Ozyweyvbydptzskit',
    correspondenceZipcode: '',
    correspondenceHouseNumber: '19',
    correspondenceHouseLletter: 'S',
    correspondenceHouseNumberSuffix: 'PC',
    correspondenceCity: 'Testgemeente',
    correspondenceCountry: 'Nederland',
    correspondenceForeignAddress1: '',
    correspondenceForeignAddress2: '',
    correspondenceForeignAddress3: '',
    phoneNumber: '0301234567',
    mobileNumber: '0687654321',
    email: 'burgerdiakriet@xxllnc.nl',
    preferenceChannel: 'webformulier',
    internalNote: 'DO NOT USE OR CHANGE!!! This person is used in the automated e2e test.'
  },
  {
    BSN: '010082426',
    firstNames: 'Gerard',
    insertions: '',
    familyName: 'Kuipers',
    nobleTitle: 'Bardon',
    birthDate: '14-05-1956',
    gender: 'Man',
    residenceCountry: 'Nederland',
    residenceStreet: 'Vaewribzxuaoqevti',
    residenceZipcode: '1721KB',
    residenceHouseNumber: '88',
    residenceHouseNumberLetter: 'Y',
    residenceHouseNumberSuffix: 'IB',
    residenceCity: 'Testgemeente',
    foreignAddress1: '',
    foreignAddress2: '',
    foreignAddress3: '',
    addressInCommunity: 'true',
    correspondenceStreet: '',
    correspondenceZipcode: '',
    correspondenceHouseNumber: '',
    correspondenceHouseLletter: '',
    correspondenceHouseNumberSuffix: '',
    correspondenceCity: '',
    correspondenceCountry: '',
    correspondenceForeignAddress1: '',
    correspondenceForeignAddress2: '',
    correspondenceForeignAddress3: '',
    phoneNumber: '0512345667',
    mobileNumber: '0699887700',
    email: 'burgerdiakriet@xxllnc.nl',
    preferenceChannel: 'email',
    internalNote: 'DO NOT USE OR CHANGE!!! This person is used in the automated e2e test.'
  },
];