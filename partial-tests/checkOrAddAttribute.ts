import { expect, Page } from '@playwright/test';
import { zsnlTestConfig } from '../playwright.config';
import { baseUrl } from '../e2e_tests';

const { typeDelay } = zsnlTestConfig;
const systemUrl = `${baseUrl}`;
let rightOneNotPresent = true;

export interface AttributeProps {
  nameAttribute?: string
  publicName?: string
  inputType?: string
  sensitiveData?: boolean
  explanationInternal?: string
  magicString?: string
  valueDefault?: string
  multipleValuesAllowed?: boolean
  relationType?: string
  objectType?: string
  docCategory?: string
  docConfidentiality?: string
  docDirection?: string
  choices?: Array<string>
  desiredFolderName?: string
}
interface Props extends AttributeProps {
  page: Page
}
export const checkOrAddAttribute = async ({
  page,
  nameAttribute = '',
  publicName = '',
  inputType = '',
  sensitiveData = false,
  magicString = nameAttribute,
  explanationInternal = '',
  valueDefault = '',
  multipleValuesAllowed = false,
  relationType = 'Contact',
  objectType = '',
  docCategory = 'inkomend',
  docConfidentiality = 'Openbaar',
  docDirection = 'intern',
  choices = [''],
  desiredFolderName = 'AUTO Kenmerken',
}: Props) => {
  await page.getByRole('link', { name: 'Catalogus', exact: true }).click();
  await page.waitForLoadState('domcontentloaded');
  await page.waitForLoadState('networkidle');
  await page.getByPlaceholder('Zoeken in de catalogus').fill(nameAttribute);
  const urlRequest2 =
    `${systemUrl}api/v2/admin/catalog/search?keyword=${encodeURIComponent(nameAttribute)}*`;
  await Promise.all([
    page.waitForRequest(urlRequest2),
    page.waitForResponse(res => res.status() === 200),
    page.getByPlaceholder('Zoeken in de catalogus').press('Enter', { delay: typeDelay }),
  ]);
  await page.waitForLoadState('domcontentloaded');
  await page.waitForLoadState('networkidle');
  const countFound = await page.getByText(nameAttribute, { exact: false }).count();
  const found = (countFound > 0);
  if (found) {
    await page.locator('button[name="buttonBarInfo"]').click();
    await page.waitForLoadState('domcontentloaded');
    for (let i = 0; i < countFound; i++) {
      const urlRequest3 = 'api\/v2\/admin\/catalog\/get_entry_detail.*';
      await Promise.all([
        page.waitForRequest(new RegExp(systemUrl + urlRequest3)),
        page.waitForResponse(res => res.status() === 200),
        page.getByRole('row', { name: 'row' }).getByLabel('').nth(i).check(),
      ]);
      await page.waitForLoadState('domcontentloaded');
      await page.waitForLoadState('networkidle');
      const isExtensionIcon = await page.locator(
        '(//*[contains(@class, "Mui-checked")]//ancestor::div[3]//span/*[@viewBox="0 0 24 24"])[2]')
        .getAttribute('data-testid');
      if (isExtensionIcon === 'ExtensionIcon') {
        const folderName = await page.locator('(//div/span/*[@viewBox="0 0 24 24"]//ancestor::div//a)[last()]').innerText();
        if (folderName === desiredFolderName) {
          await page.getByRole('row', { name: 'row' }).getByLabel('').nth(i).uncheck();
          rightOneNotPresent = false;
          break;
        }
      }
      await page.getByRole('row', { name: 'row' }).getByLabel('').nth(i).uncheck();
    }
    await page.locator('button[name="buttonBarInfo"]').click();
  }
  else {
    rightOneNotPresent = true;
  }
  await page.getByTestId('CloseIcon').first().click();
  if (rightOneNotPresent) {
    await page.getByRole('link', { name: desiredFolderName }).click();
    await page.getByTestId('AddIcon').nth(1).click();
    await page.getByRole('button', { name: 'Kenmerk' }).click();

    await page.getByPlaceholder('Naam kenmerk').click();
    await page.getByPlaceholder('Naam kenmerk').fill(nameAttribute);
    magicString !== nameAttribute ? await page.locator('//input[@name= "magic_string" ]')
      .fill(magicString) : '';
    await page.getByPlaceholder('Publieke naam').click();
    await page.getByPlaceholder('Publieke naam').fill(publicName);
    sensitiveData ? await page.getByLabel('Gevoelig gegeven').check() : '';
    await page.getByPlaceholder('Maak een keuze…').click();
    await page.getByPlaceholder('Maak een keuze…').fill(inputType);
    await page.getByPlaceholder('Maak een keuze…').press('ArrowDown');
    await page.getByPlaceholder('Maak een keuze…').press('Enter');
    if (inputType === 'Meervoudige keuze' ||
      inputType === 'Keuzelijst' ||
      inputType === 'Enkelvoudige keuze') {
      for (const choice of choices) {
        await page.getByPlaceholder('Voeg toe').fill(`${choice}`);
        await page.getByRole('button', { name: 'Voeg optie toe' }).click();
      }
    }
    if (inputType === 'Relatie') {
      await page.getByPlaceholder('Maak een keuze…').nth(1).click();
      await page.getByPlaceholder('Maak een keuze…').nth(1).fill(relationType);
      await page.getByPlaceholder('Maak een keuze…').nth(1).press('ArrowDown');
      await page.getByPlaceholder('Maak een keuze…').nth(1).press('Enter');
      if (relationType === 'Object') {
        await page.getByPlaceholder('Maak een keuze…').nth(2).click();
        await page.getByPlaceholder('Maak een keuze…').nth(2).fill(objectType);
        await expect(page.getByText(objectType)).toBeVisible();
        await page.getByPlaceholder('Maak een keuze…').nth(2).press('ArrowDown');
        await page.getByPlaceholder('Maak een keuze…').nth(2).press('Enter');
      }
    }
    multipleValuesAllowed ? await page.getByLabel('Meerdere waarden toegestaan').check() : '';
    if (inputType === 'Document') {
      await page.getByPlaceholder('Maak een keuze…').nth(1).click();
      await page.getByPlaceholder('Maak een keuze…').nth(1).fill(docCategory);
      await page.getByPlaceholder('Maak een keuze…').nth(1).press('ArrowDown');
      await page.getByPlaceholder('Maak een keuze…').nth(1).press('Enter');
      await page.getByPlaceholder('Maak een keuze…').nth(2).click();
      await page.getByPlaceholder('Maak een keuze…').nth(2).fill(docConfidentiality);
      await page.getByPlaceholder('Maak een keuze…').nth(2).press('ArrowDown');
      await page.getByPlaceholder('Maak een keuze…').nth(2).press('Enter');
      await page.getByPlaceholder('Maak een keuze…').nth(3).click();
      await page.getByPlaceholder('Maak een keuze…').nth(3).fill(docDirection);
      await page.getByPlaceholder('Maak een keuze…').nth(3).press('ArrowDown');
      await page.getByPlaceholder('Maak een keuze…').nth(3).press('Enter');
    }
    await page.locator('textarea[name="help"]').click();
    await page.locator('textarea[name="help"]').fill(explanationInternal);
    if (valueDefault !== '' && valueDefault !== 'undefined') {
      await page.locator('textarea[name="value_default"]').click();
      await page.locator('textarea[name="value_default"]').fill(valueDefault);
    }
    await page.locator('input[name="commit_message"]').click();
    await page.locator('input[name="commit_message"]').fill('Created for the Automatic e2e tests');
    await page.getByRole('button', { name: 'Opslaan' }).click();

    await expect(page.getByRole('link', { name: 'Catalogus' }).nth(0)).toBeVisible();
  }
};