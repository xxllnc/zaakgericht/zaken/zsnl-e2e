import { expect, Page } from '@playwright/test';
import { zsnlTestConfig } from '../playwright.config';
import { baseUrl } from '../e2e_tests';

const { shortTimeout, superLongTimeout } = zsnlTestConfig;

type PipLogOff = {
  page: Page
}
export const pipLogOff = async ({ page }: PipLogOff) => {
  await page.waitForLoadState('domcontentloaded');
  await page.waitForLoadState('networkidle');
  const menuAvailable = await page.locator('#pip-nav-close').getByRole('button', { name: 'Open het menu' }).isVisible({ timeout: shortTimeout });
  menuAvailable ?
    await page.locator('#pip-nav-close').getByRole('button', { name: 'Open het menu' })
      .click({ timeout: shortTimeout }) : '';
  await page.getByRole('link', { name: 'Uitloggen' }).click();
  await page.waitForLoadState('networkidle');
  const urlAuthLogout = new RegExp(`${baseUrl}pip(?:|\/login?)`);
  await expect(page).toHaveURL(urlAuthLogout, { timeout: superLongTimeout });
};