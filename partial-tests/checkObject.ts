import { Page, expect, test } from '@playwright/test';
import { testrondeDataArray } from '../testfiles/TESTRONDE/testrondeTestdata';
import { getShowObjectValue, getNoEditValue, getInputValue, getInputValueObject } from '../partial-tests';

const currencyFormat = new Intl.NumberFormat('nl-NL', {
  style: 'currency',
  currency: 'EUR',
});

interface Props {
  page: Page
  onPage: string
  testScriptName: string
  modeShowOrEditObject?: string
}
export const checkObject = async ({
  page,
  onPage,
  testScriptName,
  modeShowOrEditObject,
}: Props) => {
  for (const key in testrondeDataArray) {
    if ((testrondeDataArray[key].onPages?.findIndex(pagetype => pagetype === onPage) ?? -1) > -1 &&
      testrondeDataArray[key].inputType !== 'Geolocatie'
    ) {
      const attribute = testrondeDataArray[key];
      await test.step(`Check the value of ${attribute.nameAttribute} on the overview for ${testScriptName}`, async () => {
        modeShowOrEditObject !== 'editObject' ?
          await page.getByText(`${attribute.title}`).first().scrollIntoViewIfNeeded() :
          await page.frameLocator('#react-iframe').getByRole('heading', { name: `${attribute.title}` }).first().scrollIntoViewIfNeeded();

        const valueFound =
          modeShowOrEditObject === 'editObject' ?
            await getInputValueObject({ page, attribute, }) :
            modeShowOrEditObject === 'showObject' ?
              await getShowObjectValue({ page, attribute }) : 'unknown modeShowOrEditObject';

        const overviewValue2 = attribute.overviewValue2 !== undefined ? ' ' + attribute.overviewValue2 : '';
        const valueItShouldBe =
          attribute.overviewValue !== undefined ?
            //Because some inputtype fields in the object are not working as they are in the original form/case
            //the value shown is not the same for the Valuta fields.
            //The overviewvalue is corrected here for that difference.    
            attribute.inputType === 'Valuta' ?
              currencyFormat.format(Number(attribute.overviewValue.replace(',', '.'))) :
              `${attribute.overviewValue}${overviewValue2}` : '';

        expect(valueFound?.replace(/\s+/g, ' ').replace('string:', ''))
          .toEqual(`${attribute.inputType === 'Valuta' ? valueItShouldBe.replace(/\s+/g, '') : valueItShouldBe}`);
      });
    }
  }
};
