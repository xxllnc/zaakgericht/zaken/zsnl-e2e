import { FullResult, Reporter, TestCase, TestResult, TestStep, Suite, FullConfig, TestError } from '@playwright/test/reporter';
import { calcDuration } from '../utils/calcDuration';

class MyReporter implements Reporter {
  onBegin(config: FullConfig, suite: Suite) {
    console.log(`Starting the run with ${suite.allTests().length} tests`);
    console.log(`The config version: ${config.version}`);
  }

  onTestBegin(test: TestCase) {
    console.log(`Starting test ${test.title}`);
  }

  // onStepBegin(test: TestCase, result: TestResult, step: TestStep): void {
  //   console.log(`The test case: ${test.title} and the step: ${step.title}`);
  //   console.log(`The steps: ${step.category}, ${step.duration}, ${step.error}, ${step.location},
  //    ${step.parent}, ${step.startTime}, ${step.title}, ${step.titlePath}`);
  // }

  onStepEnd(test: TestCase, result: TestResult, step: TestStep): void {
    const miliseconds = step.duration;
    const durationFormatted = (miliseconds >= 1000) ? calcDuration({ miliseconds }) : '0:00.' + miliseconds.toString();
    console.log(`The test case: ${test.title}, step: ${step.title}, duration: ${durationFormatted}`);
  }

  onTestEnd(test: TestCase, result: TestResult) {
    const miliseconds = result.duration;
    const durationFormatted = (miliseconds >= 1000) ? calcDuration({ miliseconds }) : '0:00.' + miliseconds.toString();
    console.log(`Finished test ${test.title}: ${result.status}, duration: ${durationFormatted}`);
  }

  onError(error: TestError): void {
    console.log(`An error occured: ${error.message}, value: ${error.value}, stack: ${error.stack}`);
  }

  onEnd(result: FullResult) {
    console.log(`Finished the run: ${result.status}`);
  }
}
export default MyReporter;


