import { Page } from '@playwright/test';
import { baseUrl } from '../e2e_tests';
import { personDataDev, personDataDevelopment } from '../testfiles/personData';
import { locators } from '../utils';

let resultCheck = true;
const system = `${baseUrl
  .replace('https://', '')
  .split('.')[0]}` === 'dev' ? 'dev' : 'development';
const personData = system === 'dev' ? [...personDataDev] : [...personDataDevelopment];

type Props = {
  page: Page
  testTitle: string
  BSN: string

}
export const checkPersonDataInForm = async ({
  page,
  testTitle,
  BSN,
}: Props): Promise<boolean> => {

  const { residenceCountry,
  } = personData.find(person => person.BSN === BSN) ?? personData[2];
  const dataIndex = personData.map((el) => el.BSN).indexOf(BSN);

  async function checkSupplementaryData() {
    console.log(`Script: ${testTitle}, Checking supplementary data`);
    await checkAndIfNOKReport('phoneNumber', 'Telefoonnummer', 'Input');
    await checkAndIfNOKReport('mobileNumber', 'Telefoonnummer (mobiel)', 'Input');
    await checkAndIfNOKReport('email', 'E-mailadres*', 'Input');
  }

  async function checkResidenceAddress() {
    console.log(`Script: ${testTitle}, Checking residental address`);
    await checkAndIfNOKReport('residenceZipcode', 'Postcode');
    await checkAndIfNOKReport('residenceStreet', 'Straatnaam');
    await checkAndIfNOKReport('residenceHouseNumber', 'Huisnummer');
    await checkAndIfNOKReport('residenceHouseNumberPrefix', 'Huisnummertoevoeging');
    await checkAndIfNOKReport('residenceHouseNumberLetter', 'Huisletter');
    await checkAndIfNOKReport('residenceCity', 'Woonplaats');
  }

  async function checkForeignAddress() {
    console.log(`Script: ${testTitle}, Checking foreign residental address`);
    await checkAndIfNOKReport('foreignAddress1', 'Adresregel 1');
    await checkAndIfNOKReport('foreignAddress2', 'Adresregel 2');
    await checkAndIfNOKReport('foreignAddress3', 'Adresregel 3');
  }

  async function checkGeneralData() {
    console.log(`Script: ${testTitle}, Checking general data`);
    await checkAndIfNOKReport('firstNames', 'Voornamen');
    await checkAndIfNOKReport('insertions', 'Tussenvoegsel');
    await checkAndIfNOKReport('familyName', 'Achternaam');
    await checkAndIfNOKReport('nobleTitle', 'Adellijke titel');
  }

  async function checkAndIfNOKReport(dataField: string, label: string, inputOrShow = 'Show') {
    const emptyField = personData[dataIndex][dataField] === '' ? true : false;
    inputOrShow === 'Input' ?
      await page.getByLabel(`${label}`, { exact: true }).click()
      :
      emptyField ? '' :
        await page.locator(locators.labeledField(label)).nth(0).scrollIntoViewIfNeeded();
    const textOnForm = inputOrShow === 'Input' ?
      await page.getByLabel(`${label}`, { exact: true }).inputValue() || ''
      :
      await page.locator(locators.labeledField(label)).nth(0).innerText() || '';
    if (textOnForm !== personData[dataIndex][dataField]) {
      console.log(`Script: ${testTitle}, datacheck NOK: ${label}` +
        `  "${textOnForm}" !== "${personData[dataIndex][dataField]}"`);
      resultCheck = false;
    }
    else {
      console.log(`Script: ${testTitle}, datacheck OK: ${label} "${textOnForm}" ` +
        `=== "${personData[dataIndex][dataField]}"`);
    }
  }
  //************ end functions ************ */

  await checkGeneralData();

  const dutchResidence = residenceCountry === 'Nederland';
  dutchResidence ?
    await checkResidenceAddress() : await checkForeignAddress();

  await checkSupplementaryData();
  return resultCheck;
};