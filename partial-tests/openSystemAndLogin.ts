import { expect, Page } from '@playwright/test';
import { zsnlTestConfig } from '../playwright.config';
import { LoginUser } from '../types';
import { isUrlOpened, openPage } from '../utils';
import { pageTitle } from '../e2e_tests';
interface OpenSystemAndLoginProps {
  page: Page
  systemUrl: string
  user: LoginUser
}
const { longTimeout } = zsnlTestConfig;
const pageTitleInTest = `${pageTitle}`;

export const openSystemAndLogin = async ({ page, systemUrl, user }: OpenSystemAndLoginProps) => {
  await openPage(page, systemUrl);
  await isUrlOpened(page, pageTitleInTest);

  await expect(page.getByPlaceholder('Gebruikersnaam')).toBeVisible({ timeout: longTimeout });
  await page.getByPlaceholder('Gebruikersnaam').fill(user.name);
  await page.getByPlaceholder('Wachtwoord').fill(user.password);
  await page.getByRole('button', { name: 'Inloggen' }).click({ timeout: longTimeout });
  await expect(page).toHaveURL(new RegExp(systemUrl + '.*'));

  await page.waitForLoadState('domcontentloaded');
};