import { expect, Page } from '@playwright/test';

interface LogOff {
  page: Page
  systemUrl: string
}
export const logOff = async ({ page, systemUrl }: LogOff) => {
  const inAdministration = await page.locator('button[name="navigationBarMenu"]').isVisible({ timeout: 0 });
  if (inAdministration) {
    await page.getByRole('link', { name: 'Dashboard', exact: true }).click();
  }
  await page.locator('[aria-label="Hoofdmenu openen"]').click();
  await page.getByRole('menuitem', { name: ' Uitloggen' }).click();
  await page.waitForLoadState('networkidle');
  const urlAuthLogout = new RegExp(systemUrl + 'auth\/(?:page|login(?:\\?referer.+)?)|intern\/');
  await expect(page).toHaveURL(urlAuthLogout);

};