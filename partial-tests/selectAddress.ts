import { Page, expect, } from '@playwright/test';
import { TestrondeDataArray, } from '../testfiles/TESTRONDE/testrondeTestdata';
import { baseUrl } from '../e2e_tests';
import { zsnlTestConfig } from '../playwright.config';

const { typeDelay, superLongTimeout } = zsnlTestConfig;

interface Props {
  page: Page
  attribute: TestrondeDataArray
  fieldNumber?: number
}

const locatorPlaceholder = baseUrl.startsWith('https://dev.') ?
  'Zoek op adres (bv. Amsterdam-Duivendrecht, H.J.E. Wenckbachweg 90)' :
  'Zoek op adres (bv. Amsterdam, Ellermanstraat 23)';

export const selectAddress = async ({
  page,
  attribute,
  fieldNumber = 1
}: Props) => {
  const addressLocator = attribute.inputType === 'Adres (dmv postcode)' ||
    attribute.inputType === 'Adressen (dmv postcode)' ||
    attribute.inputType === 'Adres (dmv straatnaam)' ||
    attribute.inputType === 'Adressen (dmv straatnaam)' ||
    attribute.inputType === 'Straten' ||
    attribute.inputType === 'Straat' ?
    page.getByLabel(`${attribute.publicName}*`).first() :

    attribute.inputType === 'Locatie met kaart' ||
      attribute.inputType === 'Adres (Google Maps)' ?
      page.locator(`//vorm-field[.//text()="${attribute.publicName}"]//input`) :

      attribute.inputType === 'Adres V2' ?
        page.locator('zs-address').getByPlaceholder(`${locatorPlaceholder}`) :
        page.getByLabel(`${attribute.publicName}*`).first();

  await addressLocator.click();
  const addressToFill = fieldNumber === 1 ?
    attribute.addressValue :
    attribute.addressValue2;
  await addressLocator.type(addressToFill !== undefined ?
    addressToFill.replace('\\', '') : '3311BV 60');
  await page.waitForLoadState('networkidle');

  const addressLocatorSelect = attribute.inputType === 'Adres V2' ||
    attribute.inputType === 'Locatie met kaart' ||
    attribute.inputType === 'Adres (Google Maps)' ?
    addressLocator :
    page.getByRole('textbox', { name: `${attribute.publicName}` });
  const addressToSelectUncorrected = fieldNumber === 1 ?
    attribute.overviewValue :
    attribute.overviewValue2;
  const addressToSelect = addressToSelectUncorrected?.endsWith(' + −') ?
    addressToSelectUncorrected.substring(0, addressToSelectUncorrected.length - 4) :
    addressToSelectUncorrected;

  await expect(page.getByText(addressToSelect !== undefined ?
    addressToSelect.replace('\\', '') : 'Bunninkh\'plein 60, 3311BV Dordrecht')).toBeVisible({ timeout: superLongTimeout });
  await addressLocatorSelect.press('ArrowDown', { delay: typeDelay });
  await Promise.all([
    page.waitForRequest(new RegExp('api.pdok.nl')),
    page.waitForResponse(res => res.status() === 200),
    addressLocatorSelect.press('Enter'),
  ]);
  await page.waitForLoadState('domcontentloaded');
  await page.waitForLoadState('networkidle');
  // eslint-disable-next-line playwright/no-wait-for-timeout
  await page.waitForTimeout(300);
};