import { Page, expect, test } from '@playwright/test';
import { zsnlTestConfig } from './../playwright.config';
import { getCaseNumber } from '../utils';

const { longTimeout, superLongTimeout } = zsnlTestConfig;
let caseNumberInUrl = '';

interface Props {
  page: Page
  waitForText: string
  testScriptName: string
}

export const registerCaseAndCheckMessages = async ({
  page,
  waitForText,
  testScriptName,
}: Props) => {
  await test.step(`Register case for ${testScriptName}`, async () => {
    await page.getByRole('button', { name: 'Versturen' }).click();
  });

  await test.step(`Check case is being registered message is shown for ${testScriptName}`, async () => {
    await expect(page.locator(
      '//div[@class="snack-message" ]').filter({ hasText: 'Uw zaak wordt geregistreerd.' }).first()).toBeVisible({ timeout: superLongTimeout });
  });
  await test.step(`Check case is being registered message is hidden for ${testScriptName}`, async () => {
    await expect(page.locator(
      '//div[@class="snack-message"]').filter({ hasText: 'Uw zaak wordt geregistreerd.' }).first()).toBeHidden({ timeout: superLongTimeout });
  });

  await test.step(`Check case was registered message for ${testScriptName}`, async () => {
    await expect(page.locator('//div[@class="snack-message"]')
      .filter({ hasText: 'Zaak is door u in behandeling genomen onder zaaknummer ' }).first()
    ).toBeVisible({ timeout: superLongTimeout });
    await expect(page.locator('//div[@class="snack-message"]')
      .filter({ hasText: 'Zaak is door u in behandeling genomen onder zaaknummer ' }).first()
    ).toBeHidden({ timeout: superLongTimeout });
  });

  await test.step(`Wait for text in case to be visible for ${testScriptName}`, async () => {
    await expect(page.getByText(waitForText).first()).toBeVisible({ timeout: longTimeout });
  });

  await test.step(`Get the case number out of the url  for ${testScriptName}`, async () => {
    caseNumberInUrl = getCaseNumber({ textEndsWithCase: page.url(), seperator: '\/' });
  });
  return caseNumberInUrl
}
