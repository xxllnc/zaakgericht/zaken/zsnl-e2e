import { Page, expect, test } from '@playwright/test';
import { testrondeDataArray } from '../testfiles/TESTRONDE/testrondeTestdata';
import { getNoEditValue } from './getNoEditValue';

interface Props {
  page: Page
  onPage: string
  testScriptName: string
}
export const checkOverviewPage = async ({
  page,
  onPage,
  testScriptName
}: Props) => {
  for (const key in testrondeDataArray) {
    if ((testrondeDataArray[key].onPages?.findIndex(pagetype => pagetype === onPage) ?? -1) > -1 &&
      testrondeDataArray[key].inputType !== 'Geolocatie' &&
      testrondeDataArray[key].inputType !== 'Locatie met kaart'
    ) {
      const attribute = testrondeDataArray[key];
      const overviewExtraText = '';
      await test.step(`Check the value of ${attribute.nameAttribute} on the overview for ${testScriptName}`, async () => {
        await page.getByText(`${attribute.publicName}`, { exact: true }).scrollIntoViewIfNeeded();

        const valueFound = await getNoEditValue({ page, attribute });
        // eslint-disable-next-line playwright/no-conditional-in-test
        const overviewValue2 = attribute.overviewValue2 !== undefined ? ' ' + attribute.overviewValue2 : '';
        const valueItShouldBe =
          // eslint-disable-next-line playwright/no-conditional-in-test
          attribute.overviewValue !== undefined ?
            //Because some inputtype fields in the object are not working as they are in the original form/case
            //the value shown is not the same for Groot tekstveld fields.
            //The overviewvalue is corrected here for that difference.    
            attribute.inputType !== 'Groot tekstveld' ?
              `${attribute.overviewValue}${overviewValue2}${overviewExtraText}` :
              `${attribute.overviewValue.substring(0, attribute.overviewValue.length - 6) + '... MEER'}` :
            'no overviewValue!';
        expect(valueFound.replace(/\s+/g, ' ').replace('string:', '')).toEqual(`${valueItShouldBe}`);
      });
    }
  }
};
