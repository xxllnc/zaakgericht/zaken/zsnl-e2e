import { Page, expect, test } from '@playwright/test';
import { testrondeDataArray } from '../testfiles/TESTRONDE/testrondeTestdata';
import { getNoEditValue } from './getNoEditValue';
import { getInputValue } from './getInputValue';

interface Props {
  page: Page
  onPage: string
  testScriptName: string
}

export const checkChoiceIsYesRegistrationPage = async ({
  page,
  onPage,
  testScriptName
}: Props) => {
  for (const key in testrondeDataArray) {
    if ((testrondeDataArray[key].onPages?.findIndex(pagetype => pagetype === onPage) ?? -1) > -1 &&
      testrondeDataArray[key].valueChoiceIsYes !== undefined
    ) {
      const attribute = testrondeDataArray[key];
      await test.step(`Check the by a rule changed value of ${attribute.nameAttribute} for ${testScriptName}`, async () => {
        await page.getByText(`${attribute.publicName}`).scrollIntoViewIfNeeded();

        // eslint-disable-next-line playwright/no-conditional-in-test
        const inputValue = testScriptName.endsWith('"Ja"') && attribute.afterChoiceValueFixed ?
          await getNoEditValue({ page, attribute }) :
          await getInputValue({ attribute, page });
        expect(inputValue.replace(/\s+/g, ' ').replace('string:', '')).toEqual(`${attribute.valueChoiceIsYes}`);
      });
    }
  }
};
