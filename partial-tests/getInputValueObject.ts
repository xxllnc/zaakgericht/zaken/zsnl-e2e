import { Page, expect, } from '@playwright/test';
import { TestrondeDataArray, testrondeDataArray, } from '../testfiles/TESTRONDE/testrondeTestdata';
import { locators } from '../utils';

interface Props {
  page: Page
  attribute: TestrondeDataArray
}

export const getInputValueObject = async ({
  page,
  attribute,
}: Props): Promise<string> => {
  const valueFound1 = attribute.inputType !== 'Document' && attribute.inputType !== 'Enkelvoudige keuze'
    && attribute.inputType !== 'Valuta'
    && attribute.inputType !== 'Meervoudige keuze' && attribute.inputType !== 'Groot tekstveld' ?
    await page.frameLocator('#react-iframe').locator(`input[name="${attribute.nameAttribute}"]`).inputValue() :

    attribute.inputType === 'Groot tekstveld' ?
      await page.frameLocator('#react-iframe').locator(locators.objectInputField(`${attribute.title}`, `${attribute.inputType}`)).first().innerHTML() :

      attribute.inputType === 'Valuta' ?
        await page.frameLocator('#react-iframe').locator(locators.objectInputField(`${attribute.title}`, `${attribute.inputType}`)).first().inputValue() :

        attribute.inputType === 'Document' ?
          await page.frameLocator('#react-iframe').locator(locators.objectInputField(`${attribute.title}`, `${attribute.inputType}`)).first().innerText()
          : '';

  let valueFound2 = '';
  for (const choice of attribute.choices !== undefined ? attribute.choices : '') {
    if (attribute.inputType === 'Enkelvoudige keuze' || attribute.inputType === 'Meervoudige keuze') {
      valueFound2 = await page.frameLocator('#react-iframe').getByText(`${choice}`).isChecked() ?
        valueFound2 !== '' ? `${valueFound2} ${choice}` : `${choice}` : `${valueFound2}`;
    }
  };

  const valueFound = `${valueFound1}${valueFound2}`;
  return valueFound;
};
