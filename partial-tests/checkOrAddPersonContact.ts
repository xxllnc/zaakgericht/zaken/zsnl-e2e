import { expect, Page } from '@playwright/test';
import { baseUrl } from '../e2e_tests';
import { zsnlTestConfig } from '../playwright.config';
import { personDataDev, personDataDevelopment } from '../testfiles/personData';
import { locators, navigateAdministration } from '../utils';

const { shortTimeout, longTimeout } = zsnlTestConfig;
const system = `${baseUrl
  .replace('https://', '')
  .split('.')[0]}` === 'dev' ? 'dev' : 'development';

interface Props {
  page: Page
  testTitle: string
  BSN: string

}
export const checkOrAddPersonContact = async ({
  page,
  testTitle,
  BSN,
}: Props) => {

  let isFound = false;
  const personData = system === 'dev' ? [...personDataDev] : [...personDataDevelopment];
  const { firstNames, insertions, familyName, nobleTitle, gender,
    residenceCountry, residenceStreet, residenceZipcode, residenceHouseNumber,
    residenceHouseNumberSuffix, residenceCity,
    foreignAddress1, foreignAddress2, foreignAddress3, addressInCommunity,
    correspondenceStreet, correspondenceZipcode, correspondenceHouseNumber,
    correspondenceHouseNumberSuffix, correspondenceCity,
    correspondenceCountry, phoneNumber, mobileNumber, email,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    internalNote } = personData.find(person => person.BSN === BSN) ?? personData[0];
  const dataIndex = personData.map((el) => el.BSN).indexOf(BSN);
  const dutchResidence = residenceCountry === 'Nederland';
  const dutchCorrespondence = correspondenceCountry === 'Nederland';

  console.log(`The test case: ${testTitle}, Start of contact search`);
  await navigateAdministration({ page, menuItem: 'Contact zoeken' });

  await page.locator('input[name="bsn"]').fill(BSN);
  await page.getByRole('button', { name: 'Zoeken' }).click();

  try {
    await expect(page.getByText('Geen resultaten gevonden.')).toBeVisible({ timeout: shortTimeout });
  }
  catch {
    isFound = true;
  }

  if (isFound) {
    await openFoundContact();
    'Ja' === await page.locator(locators.labelLeftTextInContact('Authentiek')).innerText() ?
      '' : await checkSetGeneralAndAddressesData();

    await checkSetSupplementaryData();
  }
  else {
    await setGeneralData();
    dutchResidence ?
      await setResidenceAddress() : await setForeignAddress();
    await setCorrespondenceAddress();

    await saveNewContact();

    await setInternalNote();

    //Correspondence country != Nederland addres can't be set while adding a new contact
    //Add it now on the overview screen
    //Same for the house number letters
    correspondenceCountry !== '' ?
      await checkSetCountry('correspondenceCountry', correspondenceCountry) : '';

    Boolean(correspondenceCity) && await (
      dutchCorrespondence ? checkSetCorrespondenceAddress() : checkSetForeignAddress('correspondenceForeignAddress'));

    dutchResidence ?
      await checkSetResidenceAddress() : '';
    await saveContactWhenChanged();
  }

  await checkInternalNoteVisibility();
  await page.getByRole('link', { name: 'Dashboard Dashboard' }).click();

  async function checkSetGeneralAndAddressesData() {
    await checkSetGeneralData();
    await checkSetCountry('residenceCountry', residenceCountry);
    dutchResidence ?
      await checkSetResidenceAddress() : await checkSetForeignAddress('foreignAddress');

    const isCorrespondenceAddressChecked = await page.getByLabel('Ja').isChecked();
    !isCorrespondenceAddressChecked && correspondenceCity !== '' ? await page.getByLabel('Ja').check() : '';
    isCorrespondenceAddressChecked && correspondenceCity === '' ? await page.getByLabel('Nee').check() : '';
    correspondenceCountry !== '' ? await checkSetCountry('correspondenceCountry', correspondenceCountry) : '';
    Boolean(correspondenceCity) && await (
      dutchCorrespondence ? checkSetCorrespondenceAddress() : checkSetForeignAddress('correspondenceForeignAddress'));

    await saveContactWhenChanged();
  }

  //************ functions ************ */
  async function setInternalNote() {
    console.log(`The test case: ${testTitle}, Open new contact to set internal note`);
    await page.getByRole('link', { name: 'Contact bekijken' }).click();
    await page.locator('input[name="internalNote"]').fill(internalNote);
    await page.locator('button[name="saveAdditionalInfo"]').click();
  }

  async function saveNewContact() {
    console.log(`The test case: ${testTitle}, Saving the new contact`);
    await page.getByRole('button', { name: 'Aanmaken' }).click();
    await expect(page.getByText('Het contact is aangemaakt.')).toBeVisible();
  }

  async function checkInternalNoteVisibility() {
    console.log(`The test case: ${testTitle}, Check internal note`);
    await expect(page.getByText(
      'Interne notitie: "DO NOT USE OR CHANGE!!! This person is used in the automated e2e test."'))
      .toBeVisible();
  }

  async function openFoundContact() {
    console.log(`The test case: ${testTitle}, Open known contact`);
    const locie = page.locator(`//a[contains(.//text(),"${familyName}")]`);
    await locie.isVisible() ? await locie.click() : await page.locator('//div[.//text()="Naam"]/following-sibling::div//a').click();
  }

  async function saveContactWhenChanged() {
    console.log(`The test case: ${testTitle}, Save contact when changed`);
    if (await page.locator('button[name="saveCommonValues"]').isEnabled()) {
      await page.locator('button[name="saveCommonValues"]').click();
      await expect(page.getByText('De gegevens zijn succesvol opgeslagen.')).toBeVisible();
      await expect(page.getByText('De gegevens zijn succesvol opgeslagen.')).toBeHidden({ timeout: longTimeout });
    }
  }

  async function checkSetSupplementaryData() {
    console.log(`The test case: ${testTitle}, Checking and reseting supplementary data`);
    await checkFill('phoneNumber');
    await checkFill('mobileNumber');
    await checkFill('email');
    await checkFill('internalNote');

    console.log(`The test case: ${testTitle}, Save supplementary data when changed`);
    const saveAdditionalInfoEnabled = await page.locator('button[name="saveAdditionalInfo"]').isEnabled();
    if (saveAdditionalInfoEnabled) {
      await page.locator('button[name="saveAdditionalInfo"]').click();
    }
  }

  async function checkSetForeignAddress(residenceOrCorrespondenceAddress: string) {
    console.log(`The test case: ${testTitle}, Checking and reseting ${residenceOrCorrespondenceAddress} address`);
    await checkFill(`${residenceOrCorrespondenceAddress}1`);
    await checkFill(`${residenceOrCorrespondenceAddress}2`);
    await checkFill(`${residenceOrCorrespondenceAddress}3`);
  }

  async function checkSetCorrespondenceAddress() {
    if (correspondenceCity === '') { return; }

    console.log(`The test case: ${testTitle}, Checking and reseting correspondence address`);
    await checkSetCountry('correspondenceCountry', correspondenceCountry);
    await checkFill('correspondenceZipcode');
    await checkFill('correspondenceStreet');
    await checkFill('correspondenceHouseNumber');
    await checkFill('correspondenceHouseNumberSuffix');
    await checkFill('correspondenceCity');
  }

  async function checkSetCountry(residenceOrCorrespondenceCountry: string, countryItShouldBe: string) {
    console.log(`The test case: ${testTitle}, Check and set ${residenceOrCorrespondenceCountry}`);
    const country = await page.locator(`//input[@name="${residenceOrCorrespondenceCountry}"]`).getAttribute('value') || '';
    if (countryItShouldBe === country) {
      return;
    }

    await page.locator(`//input[@name="${residenceOrCorrespondenceCountry}"]`).click();
    const locieOption = await page.locator(`//input[@name="${residenceOrCorrespondenceCountry}"]`).getAttribute('id');
    await page.locator(`//input[@name="${residenceOrCorrespondenceCountry}"]`).fill(countryItShouldBe);
    await page.locator(`//li[@id="${locieOption}-option-0"]`).click();
  }

  async function setGeneralData() {
    console.log(`The test case: ${testTitle}, Adding new general data`);
    await page.getByRole('link', { name: 'Dashboard Dashboard' }).click();
    await page.hover('[aria-label="Groene plus knop menu openen"]');
    await page.getByRole('menuitem', { name: 'Contact aanmaken' }).click();
    await page.getByLabel('BSN*').fill(BSN);
    await page.getByLabel('Voornamen*').fill(firstNames);
    await page.getByLabel('Voorvoegsel*').fill(insertions);
    await page.getByLabel('Achternaam*').fill(familyName);
    await page.getByLabel('Adellijke titel*').fill(nobleTitle);
    await page.getByLabel(gender).check();

    console.log(`The test case: ${testTitle}, Adding supplementary data`);
    await fillByLabel('Telefoonnummer*', phoneNumber);
    await fillByLabel('Telefoonnummer (mobiel)*', mobileNumber);
    await fillByLabel('Emailadres*', email);
  }

  async function checkSetResidenceAddress() {
    console.log(`The test case: ${testTitle}, Checking and reseting residental address`);
    await checkFill('residenceZipcode');
    await checkFill('residenceStreet');
    await checkFill('residenceHouseNumber');
    await checkFill('residenceHouseNumber');
    await checkFill('residenceHouseNumberLetter');
    await checkFill('residenceCity');
  }

  async function setForeignAddress() {
    console.log(`The test case: ${testTitle}, Adding foreign address`);
    // eslint-disable-next-line playwright/no-conditional-in-test
    const locie = page.locator(`//select/option[.//text()="${residenceCountry}"]`);
    const optionNumber = await locie.getAttribute('value') || -1;
    await page.getByRole('combobox', { name: 'LandDit veld is verplicht' })
      .selectOption(optionNumber.toString());

    await fillByLabel('Adresregel 1*', foreignAddress1);
    await fillByLabel('Adresregel 2*', foreignAddress2);
    await fillByLabel('Adresregel 3*', foreignAddress3);
  }

  async function setResidenceAddress() {
    console.log(`The test case: ${testTitle}, Adding residential address in Nederland`);
    await fillByLabel('Postcode*', residenceZipcode);
    await fillByLabel('Huisnummer*', residenceHouseNumber);
    await fillByLabel('Huisnummertoevoeging*', residenceHouseNumberSuffix);
    await fillByLabel('Straat*', residenceStreet);
    await fillByLabel('Woonplaats*', residenceCity);

    if (addressInCommunity) {
      await page.getByLabel('Binnengemeentelijk*').check();
    }
  }

  async function setCorrespondenceAddress() {
    if (correspondenceCity === '') { return; }

    console.log(`The test case: ${testTitle}, Adding correspondence address`);
    await page.getByLabel('Briefadres*').check();
    await fillByLabel('Briefadres postcode*', correspondenceZipcode);
    await fillByLabel('Briefadres huisnummer*', correspondenceHouseNumber);
    await fillByLabel('Briefadres huisnummer toevoeging*', correspondenceHouseNumberSuffix);
    await fillByLabel('Briefadres straat*', correspondenceStreet);
    await fillByLabel('Briefadres woonplaats*', correspondenceCity);
  }

  async function checkSetGeneralData() {
    console.log(`The test case: ${testTitle}, Checking and reseting general data`);
    await checkFill('firstNames');
    await checkFill('insertions');
    await checkFill('familyName');
    await checkFill('nobleTitle');
    const isGender = await page.getByText(gender).isChecked();
    if (!isGender) {
      await page.getByText(gender).check();
    }
  }

  async function checkFill(field: string) {
    const valueInField = await page.locator(`input[name="${field}"]`).first().getAttribute('value') || '';
    if (valueInField !== personData[dataIndex][field]) {
      await page.locator(`input[name="${field}"]`).first().click();
      await page.locator(`input[name="${field}"]`).first().fill(personData[dataIndex][field]);
    }
  }

  async function fillByLabel(label: string, data: string) {
    await page.getByLabel(label).first().click();
    await page.getByLabel(label).first().fill(data);

  }
};