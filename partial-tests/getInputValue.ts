import { Page, } from '@playwright/test';
import { TestrondeDataArray, } from '../testfiles/TESTRONDE/testrondeTestdata';

interface Props {
  page: Page
  attribute: TestrondeDataArray
}

export const getInputValue = async ({
  page,
  attribute
}: Props) =>
  attribute.inputType !== 'Rich text' && attribute.inputType !== 'Groot tekstveld' &&
    attribute.inputType !== 'Enkelvoudige keuze' ?
    await page.getByLabel(`${attribute.publicName}`).inputValue() :
    // eslint-disable-next-line playwright/no-conditional-in-test
    attribute.inputType === 'Enkelvoudige keuze' ?
      await page.locator(`//*[.//text()="${attribute.publicName}"]/parent::li//input[@aria-checked="true"]/parent::label/span`)
        .inputValue() :
      // eslint-disable-next-line playwright/no-conditional-in-test
      attribute.inputType === 'Groot tekstveld' ?
        await page.locator('textarea').inputValue() :
        await page.locator('//*[@class="ql-editor"]').innerText();
