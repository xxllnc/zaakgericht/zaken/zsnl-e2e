import { expect, Page } from '@playwright/test';
import { locators } from '../utils';
import { baseUrl } from '../e2e_tests';

const defaultApplicantName = `${baseUrl
  .replace('https://', '')
  .split('.')[0]}` === 'dev' ? 'Erika de Goede' : 'Gerard Kuipers 14-05-1956';
const applicantNameToClick = defaultApplicantName.split(' ')[1];
interface Props {
  page: Page
  caseType?: string
  applicantType?: string
  applicantName?: string
  channel?: string
}
export const createNewCase = async ({
  page,
  caseType = 'Basiszaaktype dun',
  applicantType = 'Persoon',
  applicantName = defaultApplicantName,
  channel = 'email'
}: Props) => {
  await page.locator('[aria-label="Groene plus knop menu openen"]').click();
  try {
    await expect(page.getByRole('menuitem', { name: 'Zaak aanmaken' })).toBeVisible();
  }
  catch {
    await page.locator('[aria-label="Groene plus knop menu openen"]').click();
  }
  await page.getByRole('menuitem', { name: 'Zaak aanmaken' }).click();
  await page.getByLabel('Zaaktype*').fill(caseType);
  await page.locator(locators.resultFound(caseType)).click();
  await page.getByLabel(applicantType, { exact: true }).click();
  await page.getByLabel(`Aanvrager (${applicantType.toLowerCase()})*`).click();
  await page.getByLabel(`Aanvrager (${applicantType.toLowerCase()})* `).type(applicantName);
  await page.waitForResponse(res => res.status() === 200);
  await page.locator(locators.resultFound(applicantNameToClick)).click();
  await page.getByRole('combobox', { name: 'ContactkanaalDit veld is verplicht' }).selectOption(`string:${channel}`);
  try {
    await expect(page.locator('option[selected="selected"]')).toHaveValue(`string:${channel}`);
  }
  catch {
    await page.getByRole('combobox', { name: 'ContactkanaalDit veld is verplicht' }).selectOption(`string:${channel}`);
  }
  await page.getByText('Volgende').click();
  await page.waitForLoadState('domcontentloaded');
  await page.waitForLoadState('networkidle');

};