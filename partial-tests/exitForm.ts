import { Page, expect, test } from '@playwright/test';
import { zsnlTestConfig } from './../playwright.config';

const { typeDelay, } = zsnlTestConfig;
type Props = {
  page: Page
  testScriptName: string
}
export const exitForm = async ({
  page,
  testScriptName
}: Props) => {
  await test.step(`Exit ${testScriptName}`, async () => {
    await page.waitForLoadState('domcontentloaded');
    await page.waitForLoadState('networkidle');
    await page.getByRole('button', { name: 'Sluiten', exact: true }).nth(1).click();
    await page.getByRole('button', { name: 'Bevestigen' }).click({ delay: typeDelay });
    await page.waitForLoadState('domcontentloaded');
    await page.waitForLoadState('networkidle');
    await expect(page.getByText('Mijn openstaande zaken')).toBeVisible();
    const wissenPresent = await page.getByText('wissen', { exact: false }).count();
    // eslint-disable-next-line playwright/no-conditional-in-test
    wissenPresent ? await page.reload({ waitUntil: 'domcontentloaded' }) : '';
    await page.waitForLoadState('networkidle');
  });
};
