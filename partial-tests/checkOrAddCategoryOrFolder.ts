import { expect, Page } from '@playwright/test';
import { zsnlTestConfig } from '../playwright.config';
import { baseUrl } from '../e2e_tests';

const { typeDelay } = zsnlTestConfig;

interface Props {
  page: Page
  categoryOrFolder: string
}
export const checkOrAddCategoryOrFolder = async ({
  page,
  categoryOrFolder
}: Props) => {
  await page.waitForLoadState('domcontentloaded');
  await page.waitForLoadState('networkidle');
  await page.getByPlaceholder('Zoeken in de catalogus').fill(categoryOrFolder);
  const urlRequest2 = `${baseUrl}api/v2/admin/catalog/search?keyword=${encodeURIComponent(categoryOrFolder)}*`;
  await Promise.all([
    page.waitForRequest(urlRequest2),
    page.waitForResponse(res => res.status() === 200),
    page.getByPlaceholder('Zoeken in de catalogus').press('Enter', { delay: typeDelay }),
  ]);
  await page.waitForLoadState('domcontentloaded');
  await page.waitForLoadState('networkidle');
  const countFound = await page.getByText(categoryOrFolder).count();
  if (countFound === 0) {
    await page.getByTestId('CloseIcon').nth(0).click();
    await page.getByTestId('AddIcon').nth(1).click();
    await page.getByRole('button', { name: 'Map' }).click();
    await page.getByPlaceholder('Mapnaam').fill(categoryOrFolder);
    await page.getByRole('button', { name: 'Opslaan' }).click();
    try {
      await expect(page.getByRole('link', { name: 'Catalogus', exact: true })).toBeVisible();
    }
    catch {
      // await expect(page.getByText('Een map met de opgegeven naam bestaat al. Geef een andere naam op')).toBeVisible();
      await page.getByRole('button', { name: 'OK' }).click();
      await page.getByRole('button', { name: 'Annuleren' }).click();
    }
    await page.getByPlaceholder('Zoeken in de catalogus').fill(categoryOrFolder);
    await page.getByPlaceholder('Zoeken in de catalogus').press('Enter');
    await expect(page.getByRole('link', { name: categoryOrFolder })).toBeVisible();
    await expect(page.getByText('Map').first()).toBeVisible();
  }
  await page.getByTestId('CloseIcon').click();
};