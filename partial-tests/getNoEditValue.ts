import { Page, } from '@playwright/test';
import { TestrondeDataArray, } from '../testfiles/TESTRONDE/testrondeTestdata';
import { locators } from '../utils';

interface Props {
  page: Page
  attribute: TestrondeDataArray
}

export const getNoEditValue = async ({
  page,
  attribute
}: Props) =>
  await page.locator(locators.labeledField(`${attribute.publicName}`, true)).innerText();
