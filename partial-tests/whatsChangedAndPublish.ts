import { Page, expect, test } from '@playwright/test';

interface Props {
  page: Page
  whatsChangedArray: string[]
  publishText: string
  testScriptName: string
}
export const whatsChangedAndPublish = async ({
  page,
  whatsChangedArray,
  publishText,
  testScriptName
}: Props) => {

  await test.step(`Goto finish ${testScriptName} @fill_empty_db`, async () => {
    await page.frameLocator('iframe[title="Beheer formulier"]').getByRole('link', { name: 'afronden' }).click();
    await expect(page.frameLocator('iframe[title="Beheer formulier"]')
      .getByText('Componenten gewijzigd')).toBeVisible();
  });

  await test.step(`Fill publishtext ${testScriptName} @fill_empty_db`, async () => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('textbox').click();
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('textbox').fill(publishText);
  });

  await test.step(`Check the changed ${testScriptName} @fill_empty_db`, async () => {
    for (let index = 0; index < whatsChangedArray.length; index++) {
      await page.frameLocator('iframe[title="Beheer formulier"]').getByLabel(whatsChangedArray[index]).check();
    }
  });

  await test.step(`Publish changed ${testScriptName} @fill_empty_db`, async () => {
    await page.frameLocator('internal:attr=[title="Beheer formulier"i]').getByRole('button', { name: 'Publiceren' }).click();
    await page.waitForLoadState('domcontentloaded');
    await page.waitForLoadState('networkidle');
  });
};