/* eslint-disable playwright/no-conditional-in-test */
import { Page, test } from '@playwright/test';
import { testrondeDataArray } from '../testfiles/TESTRONDE/testrondeTestdata';
import { uploadDocument } from './uploadDocument';
import { selectAddress } from './selectAddress';

interface Props {
  page: Page
  onPage: string
  testScriptName: string
}

export const fillRegistrationPage = async ({
  page,
  onPage,
  testScriptName
}: Props) => {
  for (const key in testrondeDataArray) {
    if ((testrondeDataArray[key].onPages?.findIndex(pagetype => pagetype === onPage) ?? -1) > -1 &&
      !(testrondeDataArray[key].afterChoiceValueFixed === true &&
        testScriptName.endsWith('"Ja"'))
    ) {
      const attribute = testrondeDataArray[key];
      await test.step(`Fill, upload, select or checkmark ${attribute.publicName} for ${testScriptName}`, async () => {
        await page.getByText(`${attribute.publicName}`).first().scrollIntoViewIfNeeded();

        attribute.fillValue !== undefined ?
          await page.getByLabel(`${attribute.publicName}`)
            .fill(attribute.fillValue) :

          attribute.docCategory !== undefined ?
            await uploadDocument({ page, attribute }) :

            attribute.checkValue !== undefined ?
              await page.getByLabel(`${attribute.checkValue}`)
                .check() :

              attribute.addressValue !== undefined ?
                await selectAddress({ page, attribute }) :

                attribute.selectValue !== undefined ?
                  await page.getByRole('combobox', { name: `${attribute.publicName}` })
                    .selectOption(`string:${attribute.selectValue}`) : '';

        attribute.checkValue2 !== undefined ?
          await page.getByLabel(`${attribute.checkValue2}`)
            .click() : '';

        attribute.addressValue2 !== undefined ?
          await selectAddress({ page, attribute, fieldNumber: 2 }) : '';
      });
    }
  }

  await test.step(`Go to page Afronden for ${testScriptName}`, async () => {
    await page.getByRole('button', { name: 'Volgende', exact: true }).dblclick();
    await page.waitForLoadState('domcontentloaded');
    await page.waitForLoadState('networkidle');
  });
};
