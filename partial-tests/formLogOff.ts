import { expect, Page } from '@playwright/test';
import { zsnlTestConfig } from '../playwright.config';
import { baseUrl } from '../e2e_tests';

const { longTimeout } = zsnlTestConfig;

type FormLogOff = {
  page: Page
}
export const formLogOff = async ({ page }: FormLogOff) => {
  await page.getByRole('link', { name: '' }).click();
  await page.waitForLoadState('networkidle');
  const urlAuthLogout = new RegExp(`${baseUrl}(?:auth\/digid\/logout|pip\/login?)`);
  try {
    await expect(page).toHaveURL(urlAuthLogout, { timeout: longTimeout });
  }
  catch {
    await page.getByRole('link', { name: '' }).click();
  }
};