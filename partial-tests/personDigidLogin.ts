import { BrowserContext, expect, Page } from '@playwright/test';
import { locators, openPage } from '../utils';
import { baseUrl } from '../e2e_tests';
import { format } from 'date-fns';
import { zsnlTestConfig } from '../playwright.config';

const { shortTimeout, superLongTimeout, superShortTimeout, extreemLongTimeout } = zsnlTestConfig;

type PersonPipDigidLogin = {
  page: Page
  BSN: string
  context: BrowserContext
  testTitle: string
}

export const personPipDigidLogin = async ({ page, BSN, context, testTitle }: PersonPipDigidLogin) => {
  await openPage(page, `${baseUrl}/pip`);
  await retrySpoofUntilSucceeded(page, BSN, context, testTitle);
};

type PersonFormDigidLogin = {
  page: Page
  formName: string
  BSN: string
  context: BrowserContext
  testTitle: string
}

export const personFormDigidLogin = async ({ page, formName, BSN, context, testTitle }: PersonFormDigidLogin) => {
  await openPage(page, `${baseUrl}/form`);

  await page.locator(locators.persoonLink(formName)).click({ timeout: superLongTimeout });
  return await retrySpoofUntilSucceeded(page, BSN, context, testTitle);
};

export const retrySpoofUntilSucceeded = async (page: Page, BSN: string, context: BrowserContext, testTitle: string) => {
  await expect.poll(async () => {

    async function checkStuck() {
      errorMessageIsShown = true;
      errorMessage = 'Error message is shown';
      await expect(page.getByText('Er is een fout opgetreden')).toBeHidden({ timeout: 3 * superShortTimeout });
      errorMessageIsShown = false;

      errorMessage = 'Digid inlog button is still visible';
      await expect(page.getByRole('link', { name: 'Inloggen met SAML IdP: DigiD development' })).toBeHidden({ timeout: superShortTimeout });

      errorMessage = 'Inlog screen for employees is shown';
      await expect(page.getByText('Inloggen met Azure dev (MS DEV account)').first()).toBeHidden({ timeout: superShortTimeout });

      errorMessage = 'Maintenance screen is shown';
      await expect(page.getByText('Op dit moment voeren wij onderhoud uit aan ons zaaksysteem'))
        .toBeHidden({ timeout: superShortTimeout });
    }

    const cookies = await context.cookies(`${baseUrl}`);
    const isXSRFTOKEN = cookies.find(({ name }) => name === 'XSRF-TOKEN') ?? '';
    isXSRFTOKEN === '' ?
      console.log(`The cookies for ${testTitle}:` + JSON.stringify(cookies)) :
      console.log(`The isXSRFTOKEN for ${testTitle}:` + isXSRFTOKEN);

    let errorMessageIsShown = false;
    let errorMessage = 'There is an error';
    try {
      errorMessage = 'Maintenance screen is shown';
      await expect(page.getByText('Op dit moment voeren wij onderhoud uit aan ons zaaksysteem'))
        .toBeHidden({ timeout: superShortTimeout });

      await page.getByRole('link', { name: 'Inloggen met SAML IdP: DigiD development' }).click();
      await checkStuck();

      await page.getByLabel('BSN:').fill(BSN, { timeout: superLongTimeout });
      await page.getByRole('button', { name: 'Spoof!' })
        .click({ timeout: extreemLongTimeout });
      await page.waitForLoadState('domcontentloaded');
      await page.waitForLoadState('networkidle');

      await checkStuck();

      console.log(`No error for ${testTitle}`);
      return true;
    }
    catch {
      console.log(`Error ${testTitle}: ${errorMessage} ${format(new Date(), 'yyyy-MM-dd HH:mm:ss.SSS')}`);
      const spoofTimeout = await page.getByText('Spoof').isVisible();

      if (errorMessageIsShown || spoofTimeout) {
        try {
          console.log(`Try gobacks for ${testTitle}`);
          await page.goBack();
          try {
            await expect(page.getByRole('link', { name: 'Inloggen met SAML IdP: DigiD development' }))
              .toBeVisible({ timeout: shortTimeout });
          }
          catch {
            await page.goBack();
          }
        }
        catch {
          const urlReloaded = page.url();
          console.log(`Reload 1 url for ${testTitle}: ${urlReloaded}`);
          await page.reload();
        }
      }
      else {
        try {
          await expect(page.getByRole('link', { name: 'Inloggen met SAML IdP: DigiD development' }))
            .toBeVisible({ timeout: shortTimeout });
          const urlReloaded = page.url();
          console.log(`Reload url for ${testTitle}: ${urlReloaded}`);
          await page.reload();
        }
        catch {
          await page.goBack();
          try {
            await expect(page.getByRole('link', { name: 'Inloggen met SAML IdP: DigiD development' }))
              .toBeVisible({ timeout: shortTimeout });
          }
          catch {
            await page.goBack();
          }
        }
      }
      return false;
    }
  }, {
    // Probe, wait 2s, probe, wait 2s, probe, wait 1s, probe, wait 1s, probe, .... Defaults to [100, 250, 500, 1000].
    intervals: [2_000, 2_000, 1_000],
    timeout: 200_000
  }).toBeTruthy();

  const cookies = await context.cookies(`${baseUrl}`);
  console.log(`The cookies for ${testTitle}:` + JSON.stringify(cookies));
};
