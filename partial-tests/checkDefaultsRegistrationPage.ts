import { Page, expect, test } from '@playwright/test';
import { testrondeDataArray } from '../testfiles/TESTRONDE/testrondeTestdata';
import { getInputValue } from './getInputValue';

interface Props {
  page: Page
  onPage: string
  testScriptName: string
}

export const checkDefaultsRegistrationPage = async ({
  page,
  onPage,
  testScriptName
}: Props) => {
  for (const key in testrondeDataArray) {
    if ((testrondeDataArray[key].onPages?.findIndex(pagetype => pagetype === onPage) ?? -1) > -1 &&
      testrondeDataArray[key].valueDefault !== undefined) {
      const attribute = testrondeDataArray[key];
      await test.step(`Check the default value of ${attribute.nameAttribute} for ${testScriptName}`, async () => {
        await page.getByText(`${attribute.publicName}`).scrollIntoViewIfNeeded();
        const inputValue = await getInputValue({ attribute, page });
        expect(inputValue.replace(/\s+/g, ' ').replace('string:', '')).toEqual(`${attribute.valueDefault}`);
      });
    }
  }
};


