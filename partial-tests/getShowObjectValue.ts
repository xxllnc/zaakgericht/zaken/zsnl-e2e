import { Page, } from '@playwright/test';
import { TestrondeDataArray, } from '../testfiles/TESTRONDE/testrondeTestdata';
import { locators } from '../utils';

interface Props {
  page: Page
  attribute: TestrondeDataArray
}

export const getShowObjectValue = async ({
  page,
  attribute
}: Props) => attribute.inputType !== 'Groot tekstveld' ?
    await page.locator(locators.objectShowField(`${attribute.title}`, attribute.inputType)).innerText() :
    await page.locator(locators.objectShowField(`${attribute.title}`, attribute.inputType)).innerHTML();