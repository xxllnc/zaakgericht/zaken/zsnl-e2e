import { Page, expect, } from '@playwright/test';
import { TestrondeDataArray, testFiles } from '../testfiles/TESTRONDE/testrondeTestdata';

interface Props {
  page: Page
  attribute: TestrondeDataArray
}

const testFileDir = './testfiles/';
export const uploadDocument = async ({
  page,
  attribute
}: Props) => {
  const fileChooserPromise = page.waitForEvent('filechooser');
  await page.getByRole('listitem')
    .filter({ hasText: `${attribute.publicName}* Bestand toevoegen Of sleep je bestanden hierheen Veld t` })
    .getByRole('button', { name: 'Bestand toevoegen' }).click();
  const fileChooser = await fileChooserPromise;
  await fileChooser.setFiles(testFileDir + `${attribute.testDoc !== undefined ? attribute.testDoc : testFiles[1]}`);
  await expect(page.getByText(`${attribute.testDoc !== undefined ? attribute.testDoc : testFiles[1]}`)).toBeVisible();
  await page.waitForLoadState('networkidle');
};